
[![pipeline status](https://gitlab.com/gibranbrahmanta/wartegin/badges/master/pipeline.svg)](https://gitlab.com/gibranbrahmanta/wartegin/-/commits/master) [![coverage report](https://gitlab.com/gibranbrahmanta/wartegin/badges/master/coverage.svg)](https://gitlab.com/gibranbrahmanta/wartegin/-/commits/master)

# WartegIn!

## Advance Programming Kelas B - Kelompok 12

### Anggota:
   1. Amrisandha P. Prasetyo (1806205400)
   2. Gibran Brahmanta P. (1806186553)
   3. Muhammad Zuhdi Zamrud (1806141385)
   4. Nunun Hidayah (1806141403)
   5. Willy Sandi Harsono (180620578)


### Deskripsi WartegIn!
   WartegIn! adalah chatbot LINE yang memiliki fitur selayaknya pelayan pada suatu restoran sehingga dapat melihat menu serta memesannya.
   
   <a href="https://lin.ee/6f7Lt5q"><img height="36" border="0" alt="Tambah Teman" src="https://scdn.line-apps.com/n/line_add_friends/btn/en.png"></a>

    
### Pembagian Tugas
   1. Amrisandha P. Prasetyo - State Pattern & UI AnterIn!, PromoIn! 
   2. Gibran Brahmanta P. - Composite Pattern PesenIn!, Profiling
   3. Muhammad Zuhdi Zamrud - State Pattern & UI TanyaIn!, PesenIn! 
   4. Nunun Hidayah - PromoIn!
   5. Willy Sandi Harsono - AnterIn! 
