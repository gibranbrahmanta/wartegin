package com.herokuapp.wartegin.composite;

import java.util.ArrayList;
import java.util.Iterator;

public class Menu implements MenuComponent {

    private final ArrayList<MenuComponent> menuComponents = new ArrayList<MenuComponent>();
    private final String name;
    private String description;

    public Menu(String name) {
        this.name = name;
        this.description = "";
    }

    public void add(MenuComponent menuComponent) {
        menuComponents.add(menuComponent);
    }

    public void remove(String name) {
        Iterator<MenuComponent> iterator = menuComponents.iterator();

        while (iterator.hasNext()) {
            MenuComponent menuComponent = iterator.next();
            if (menuComponent.getName().toLowerCase().equals(name.toLowerCase())) {
                menuComponents.remove(menuComponent);
                break;
            }
        }

    }

    @Override
    public int getPrice() {
        int totalPrice = 0;
        Iterator<MenuComponent> iterator = menuComponents.iterator();

        while (iterator.hasNext()) {
            MenuComponent menuComponent = iterator.next();
            totalPrice += menuComponent.getPrice();
        }
        return totalPrice;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public String getDescription() {
        Iterator<MenuComponent> iterator = menuComponents.iterator();
        this.description = "";
        while (iterator.hasNext()) {
            MenuComponent menuComponent = iterator.next();
            this.description += menuComponent.getName() + "\n";
        }
        return this.description;
    }

    public int getTotalMenuComponents() {
        return this.menuComponents.size();
    }
}
