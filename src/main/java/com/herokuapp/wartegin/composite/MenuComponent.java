package com.herokuapp.wartegin.composite;

public interface MenuComponent {

    int getPrice();

    String getName();

}
