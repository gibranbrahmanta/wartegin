package com.herokuapp.wartegin.model;

public class Promo {
    private String name;
    private String description;
    private long count;
    private long amount;
    private double percent;

    public Promo(String name, String description, long count, long amount, double percent) {
        this.name = name;
        this.description = description;
        this.count = count;
        this.amount = amount;
        this.percent = percent;
    }

    public String getName() {
        return name;
    }

    public void setId(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public void addCount(long count) {
        this.count += count;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

}