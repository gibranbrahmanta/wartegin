package com.herokuapp.wartegin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "line_user")
public class LineUser {

    @Id
    @Column(name = "lineId", unique = true, nullable = false)
    private String lineId;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private int type;


    public LineUser() {

    }

    public LineUser(String name, String lineId, int type) {
        this.name = name;
        this.lineId = lineId;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getLineId() {
        return this.lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

}