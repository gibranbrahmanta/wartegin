package com.herokuapp.wartegin.model;

public class Driver {

    private final String name;
    private final String plateNumber;

    public Driver(String name, String plateNumber) {
        this.name = name;
        this.plateNumber = plateNumber;
    }

    public String getName() {
        return name;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

}
