package com.herokuapp.wartegin.messages;

import com.herokuapp.wartegin.composite.Menu;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.flex.unit.FlexPaddingSize;
import java.net.URI;
import java.util.Arrays;
import java.util.function.Supplier;

public class OrderListFlexMessageSupplier implements Supplier<FlexMessage> {

    private final Menu menu;

    public OrderListFlexMessageSupplier(Menu menu) {
        this.menu = menu;
    }

    @Override
    public FlexMessage get() {

        final Box header = this.createHeaderBlock();

        final Box body = this.createBodyBlock();

        final Box footer = createFooterBlock();

        final Bubble bubble = Bubble.builder()
                .header(header)
                .body(body)
                .footer(footer)
                .build();

        return new FlexMessage("Berikut ini adalah pesanan Anda.", bubble);
    }

    private Box createHeaderBlock() {
        String headerUrl = "https://i.ibb.co/YTvx1fD/pesanan-anda-3x1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(headerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }


    private Box createBodyBlock() {
        final Text orderDescription = Text.builder()
                .text(String.format("%s", this.menu.getDescription()))
                .color("#000000")
                .gravity(FlexGravity.CENTER)
                .align(FlexAlign.START)
                .size(FlexFontSize.SM)
                .wrap(true)
                .build();

        final Text orderPrice = Text.builder()
                .text(String.format("Harga Total: Rp%d", this.menu.getPrice()))
                .weight(Text.TextWeight.BOLD)
                .color("#65BF71")
                .gravity(FlexGravity.CENTER)
                .align(FlexAlign.START)
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        final Box orderBox = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(orderDescription, orderPrice))
                .build();


        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(orderBox)
                .margin(FlexMarginSize.NONE)
                .build();
    }

    private Box createFooterBlock() {
        String footerUrl = "https://i.ibb.co/jvsQ4gZ/wartegin-logo-3x1-1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(footerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }

}
