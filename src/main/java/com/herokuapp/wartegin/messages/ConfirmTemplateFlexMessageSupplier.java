package com.herokuapp.wartegin.messages;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.TemplateMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.flex.unit.FlexPaddingSize;
import com.linecorp.bot.model.message.template.ConfirmTemplate;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class ConfirmTemplateFlexMessageSupplier implements Supplier<FlexMessage> {

    private final String label;
    private final String acceptCommand;
    private final String acceptMessage;
    private final String rejectCommand;
    private final String rejectMessage;

    public ConfirmTemplateFlexMessageSupplier(String label,
                                              String acceptCommand, String acceptMessage,
                                              String rejectCommand, String rejectMessage) {
        this.label = label;
        this.acceptCommand = acceptCommand;
        this.acceptMessage = acceptMessage;
        this.rejectCommand = rejectCommand;
        this.rejectMessage = rejectMessage;
    }

    @Override
    public FlexMessage get() {
        final Box header = this.createHeaderBlock();
        final Box body = this.createBodyBlock();
        final Box footer = createFooterBlock();

        final Bubble bubble = Bubble.builder()
                .header(header)
                .body(body)
                .footer(footer)
                .build();

        return new FlexMessage("Silakan mengkonfirmasi.", bubble);
    }


    private Box createHeaderBlock() {
        List<FlexComponent> header = new ArrayList<>();

        final Text title = Text.builder()
                .text(this.label)
                .color("#FFFFFF")
                .size(FlexFontSize.Md)
                .align(FlexAlign.CENTER)
                .weight(TextWeight.BOLD)
                .wrap(true)
                .build();

        header.add(title);

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .backgroundColor("#65BF71")
                .contents(header)
                .build();
    }


    private Box createBodyBlock() {
        final PostbackAction acceptButtonPostback = PostbackAction.builder()
                .label("Ya")
                .data(String.format(
                        "{\"command\":\"%s\"}",
                        this.acceptCommand
                ))
                .displayText(
                        String.format(this.acceptMessage))
                .build();

        final Button acceptButton = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#65BF71")
                .gravity(FlexGravity.CENTER)
                .action(acceptButtonPostback)
                .build();

        final PostbackAction rejectButtonPostback = PostbackAction.builder()
                .label("Tidak")
                .data(String.format(
                        "{\"command\":\"%s\"}",
                        this.rejectCommand
                ))
                .displayText(
                        String.format(this.rejectMessage))
                .build();

        final Button rejectButton = Button.builder()
                .style(Button.ButtonStyle.SECONDARY)
                .gravity(FlexGravity.CENTER)
                .action(rejectButtonPostback)
                .build();

        final Box buttonBox = Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Arrays.asList(acceptButton, rejectButton))
                .spacing(FlexMarginSize.SM)
                .build();


        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.MD)
                .contents(buttonBox)
                .build();
    }

    private Box createFooterBlock() {
        String footerUrl = "https://i.ibb.co/jvsQ4gZ/wartegin-logo-3x1-1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(footerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }
}
