package com.herokuapp.wartegin.messages;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.model.Promo;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.flex.unit.FlexPaddingSize;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class AboutWarteginFlexMessageSupplier implements Supplier<FlexMessage> {

    @Override
    public FlexMessage get() {

        final Box header = this.createHeaderBlock();

        final Box body = this.createBodyBlock();

        final Box footer = createFooterBlock();

        final Bubble bubble = Bubble.builder()
                .header(header)
                .body(body)
                .footer(footer)
                .build();

        return new FlexMessage("Berikut adalah mengenai WartegIn!", bubble);
    }

    private Box createHeaderBlock() {
        String headerUrl = "https://i.ibb.co/WkzRH1g/tentang-wartegin-3x1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(headerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }


    private Box createBodyBlock() {
        final Text aboutWarteginTitle = Text.builder()
                .text(String.format("Selamat datang di WartegIn!"))
                .weight(Text.TextWeight.BOLD)
                .color("#65BF71")
                .gravity(FlexGravity.CENTER)
                .align(FlexAlign.CENTER)
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        final Text aboutWarteginText = Text.builder()
                .text("WartegIn! adalah chatbot LINE di mana Anda dapat memesan makanan"
                        + " dan minuman yang dapat diantar ke tempat Anda!"
                        + "\n Dikembangkan oleh M. Zuhdi Zamrud, Amrisandha P. Prasetyo, "
                        + "Gibran Brahmanta P., Willy Sandi H., dan Nunun Hidayah.")
                .color("#000000")
                .size(FlexFontSize.SM)
                .align(FlexAlign.CENTER)
                .margin(FlexMarginSize.SM)
                .wrap(true)
                .build();


        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(aboutWarteginTitle, aboutWarteginText)
                .margin(FlexMarginSize.NONE)
                .build();
    }

    private Box createFooterBlock() {
        String footerUrl = "https://i.ibb.co/jvsQ4gZ/wartegin-logo-3x1-1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(footerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }


}
