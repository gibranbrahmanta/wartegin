package com.herokuapp.wartegin.messages.state;

import com.linecorp.bot.model.message.TextMessage;

public interface OrderStateMessages {
    TextMessage getAddItemMessage();

    TextMessage getDeleteItemMessage();

    TextMessage getFinishOrderMessage();

    TextMessage getConfirmOrderMessage();

    TextMessage getCancelOrderMessage();

    TextMessage getMakeOrderMessage();

    TextMessage getShowOrderMessage();

    TextMessage getTryAgainLaterMessage();

    TextMessage getUsePromoMessage();

    TextMessage getUsedPromoMessage();

    TextMessage getCancelPromoMessage();
}
