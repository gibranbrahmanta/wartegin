package com.herokuapp.wartegin.messages.state;

import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

@Component
public class IdleOrderStateMessages implements OrderStateMessages {

    private final TextMessage ADD_ITEM_MESSAGE =
            new TextMessage("Anda harus membuat pesanan terlebih"
                    + " dahulu untuk menambahkan item menu ke dalam pesanan Anda.");
    private final TextMessage DELETE_ITEM_MESSAGE =
            new TextMessage("Anda harus membuat pesanan terlebih"
                    + " dahulu untuk menghapus item menu dari pesanan Anda.");
    private final TextMessage FINISH_ORDER_MESSAGE = new TextMessage(
            "Anda harus membuat dan mengkonfirmasi pesanan lalu"
                    + " menunggu pesanan Anda selesai kami antar.");
    private final TextMessage CONFIRM_ORDER_MESSAGE =
            new TextMessage("Anda harus membuat pesanan terlebih dahulu"
                    + " untuk mengkonfirmasi pesanan Anda.");
    private final TextMessage CANCEL_ORDER_MESSAGE =
            new TextMessage("Anda harus membuat pesanan terlebih dahulu"
                    + " untuk membatalkan pesanan Anda.");
    private final TextMessage MAKE_ORDER_MESSAGE =
            new TextMessage("Berikut adalah menu kami! Anda dapat"
                    + " memesan item menu kesukaan Anda!");
    private final TextMessage SHOW_ORDER_LIST_MESSAGE =
            new TextMessage("Anda belum melakukan pemesanan.");
    private final TextMessage TRY_AGAIN_LATER_MESSAGE =
            new TextMessage("Semua driver kami sedang sibuk sekarang. Silakan coba sesaat lagi.");
    private final TextMessage USE_PROMO_MESSAGE =
            new TextMessage(
                    "Anda harus membuat pesanan terlebih dahulu untuk menggunakan promo kami.");
    private final TextMessage USED_PROMO_MESSAGE =
            new TextMessage(
                    "Anda harus membuat pesanan terlebih dahulu untuk menggunakan promo kami.");
    private final TextMessage CANCEL_PROMO_MESSAGE =
            new TextMessage(
                    "Anda harus membuat pesanan terlebih dahulu untuk membatalkan promo kami.");


    @Override
    public TextMessage getAddItemMessage() {
        return this.ADD_ITEM_MESSAGE;
    }

    @Override
    public TextMessage getDeleteItemMessage() {
        return this.DELETE_ITEM_MESSAGE;
    }

    @Override
    public TextMessage getFinishOrderMessage() {
        return this.FINISH_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getConfirmOrderMessage() {
        return this.CONFIRM_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getCancelOrderMessage() {
        return this.CANCEL_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getMakeOrderMessage() {
        return this.MAKE_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getShowOrderMessage() {
        return this.SHOW_ORDER_LIST_MESSAGE;
    }

    @Override
    public TextMessage getTryAgainLaterMessage() {
        return this.TRY_AGAIN_LATER_MESSAGE;
    }

    @Override
    public TextMessage getUsePromoMessage() {
        return this.USE_PROMO_MESSAGE;
    }

    @Override
    public TextMessage getUsedPromoMessage() {
        return this.USED_PROMO_MESSAGE;
    }

    @Override
    public TextMessage getCancelPromoMessage() {
        return this.CANCEL_PROMO_MESSAGE;
    }

}
