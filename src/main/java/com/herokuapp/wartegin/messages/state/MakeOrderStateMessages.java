package com.herokuapp.wartegin.messages.state;

import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

@Component
public class MakeOrderStateMessages implements OrderStateMessages {
    private final TextMessage FINISH_ORDER_MESSAGE = new TextMessage(
            "Anda harus mengkonfirmasi pesanan lalu menunggu pesanan Anda selesai kami antar.");
    private final TextMessage MAKE_ORDER_MESSAGE =
            new TextMessage(
                    "Anda sedang membuat pesanan. "
                            + "Silakan tambahkan item menu ke dalam pesanan Anda.");
    private final TextMessage CONFIRM_ORDER_MESSAGE = new TextMessage(
            "Pesanan Anda berhasil dikonfirmasi dan akan kami proses.");
    private final TextMessage CANCEL_ORDER_MESSAGE =
            new TextMessage("Pesanan Anda berhasil dibatalkan.");
    private final TextMessage DELETE_ITEM_MESSAGE =
            new TextMessage("Item menu berhasil dihapus dari pesanan Anda.");
    private final TextMessage TRY_AGAIN_LATER_MESSAGE =
            new TextMessage("Semua driver kami sedang sibuk sekarang. Silakan coba sesaat lagi.");
    private final TextMessage USE_PROMO_MESSAGE =
            new TextMessage("Anda harus mengkonfirmasi pesanan terlebih "
                    + "dahulu untuk menggunakan promo kami.");
    private final TextMessage USED_PROMO_MESSAGE =
            new TextMessage("Anda harus mengkonfirmasi pesanan terlebih "
                    + "dahulu untuk menggunakan promo kami.");
    private final TextMessage CANCEL_PROMO_MESSAGE =
            new TextMessage("Anda harus mengkonfirmasi pesanan terlebih "
                    + "dahulu untuk membatalkan promo kami.");

    @Override
    public TextMessage getAddItemMessage() {
        return null;
    }

    @Override
    public TextMessage getDeleteItemMessage() {
        return this.DELETE_ITEM_MESSAGE;
    }

    @Override
    public TextMessage getFinishOrderMessage() {
        return this.FINISH_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getConfirmOrderMessage() {
        return this.CONFIRM_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getCancelOrderMessage() {
        return this.CANCEL_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getMakeOrderMessage() {
        return this.MAKE_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getShowOrderMessage() {
        return null;
    }

    @Override
    public TextMessage getTryAgainLaterMessage() {
        return this.TRY_AGAIN_LATER_MESSAGE;
    }

    @Override
    public TextMessage getUsePromoMessage() {
        return this.USE_PROMO_MESSAGE;
    }

    @Override
    public TextMessage getUsedPromoMessage() {
        return this.USED_PROMO_MESSAGE;
    }

    @Override
    public TextMessage getCancelPromoMessage() {
        return this.CANCEL_PROMO_MESSAGE;
    }

}
