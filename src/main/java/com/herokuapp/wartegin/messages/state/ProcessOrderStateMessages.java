package com.herokuapp.wartegin.messages.state;

import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

@Component
public class ProcessOrderStateMessages implements OrderStateMessages {

    private final TextMessage ADD_ITEM_MESSAGE = new TextMessage(
            "Anda harus menyelesaikan pesanan Anda dan membuat pesanan terlebih dahulu.");
    private final TextMessage DELETE_ITEM_MESSAGE = new TextMessage(
            "Anda harus menyelesaikan pesanan Anda dan membuat pesanan terlebih dahulu.");
    private final TextMessage MAKE_ORDER_MESSAGE =
            new TextMessage("Anda masih terdapat pesanan yang belum selesai.");
    private final TextMessage FINISH_ORDER_MESSAGE =
            new TextMessage("Pesanan Anda selesai. Terimakasih telah menggunakan WartegIn!");
    private final TextMessage CONFIRM_ORDER_MESSAGE =
            new TextMessage("Pesanan Anda sedang kami proses.");
    private final TextMessage CANCEL_ORDER_MESSAGE = new TextMessage(
            "Anda tidak dapat membatalkan pesanan Anda karena sedang kami proses.");
    private final TextMessage TRY_AGAIN_LATER_MESSAGE =
            new TextMessage(
                    "Semua driver kami sedang sibuk sekarang. Silakan mencoba sesaat lagi.");
    private final TextMessage USE_PROMO_MESSAGE =
            new TextMessage(
                    "Anda tidak dapat menggunakan promo. Anda sudah mengkonfirmasi pesanan Anda.");
    private final TextMessage USED_PROMO_MESSAGE =
            new TextMessage(
                    "Anda tidak dapat menggunakan promo. Anda sudah mengkonfirmasi pesanan Anda.");
    private final TextMessage CANCEL_PROMO_MESSAGE =
            new TextMessage(
                    "Anda tidak dapat membatalkan promo. Anda sudah mengkonfirmasi pesanan Anda.");

    @Override
    public TextMessage getAddItemMessage() {
        return this.ADD_ITEM_MESSAGE;
    }

    @Override
    public TextMessage getDeleteItemMessage() {
        return this.DELETE_ITEM_MESSAGE;
    }

    @Override
    public TextMessage getFinishOrderMessage() {
        return this.FINISH_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getConfirmOrderMessage() {
        return this.CONFIRM_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getCancelOrderMessage() {
        return this.CANCEL_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getMakeOrderMessage() {
        return this.MAKE_ORDER_MESSAGE;
    }

    @Override
    public TextMessage getShowOrderMessage() {
        return null;
    }

    @Override
    public TextMessage getTryAgainLaterMessage() {
        return this.TRY_AGAIN_LATER_MESSAGE;
    }

    @Override
    public TextMessage getUsePromoMessage() {
        return this.USE_PROMO_MESSAGE;
    }

    @Override
    public TextMessage getUsedPromoMessage() {
        return this.USED_PROMO_MESSAGE;
    }

    @Override
    public TextMessage getCancelPromoMessage() {
        return this.CANCEL_PROMO_MESSAGE;
    }

}
