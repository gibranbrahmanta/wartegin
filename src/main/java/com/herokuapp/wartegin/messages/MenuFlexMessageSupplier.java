package com.herokuapp.wartegin.messages;

import com.herokuapp.wartegin.model.Menu;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexBorderWidthSize;
import com.linecorp.bot.model.message.flex.unit.FlexCornerRadiusSize;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.flex.unit.FlexPaddingSize;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class MenuFlexMessageSupplier implements Supplier<FlexMessage> {

    private final List<Menu> foodList;
    private final List<Menu> drinkList;
    private final String addCommand;
    private final String deleteCommand;
    private String type;

    public MenuFlexMessageSupplier(List<Menu> foodList, List<Menu> drinkList) {
        this.foodList = foodList;
        this.drinkList = drinkList;
        this.addCommand = "addItem";
        this.deleteCommand = "deleteItem";
    }

    @Override
    public FlexMessage get() {

        final Box foodHeader = this.createHeaderBlock("https://i.ibb.co/Fz0JrGh/menu-makanan-3x1.png");

        final Box foodBody = this.createBodyBlock(this.foodList);

        final Box footer = createFooterBlock();

        final Bubble foodBubble = Bubble.builder()
                .header(foodHeader)
                .body(foodBody)
                .footer(footer)
                .build();

        final Box drinkHeader = this.createHeaderBlock("https://i.ibb.co/bF4HGcd/menu-minuman-3x1.png");

        final Box drinkBody = this.createBodyBlock(this.drinkList);

        final Bubble drinkBubble = Bubble.builder()
                .header(drinkHeader)
                .body(drinkBody)
                .footer(footer)
                .build();

        final Carousel carousel = Carousel.builder()
                .contents(Arrays.asList(foodBubble, drinkBubble))
                .build();

        return new FlexMessage("Berikut adalah menu WartegIn!", carousel);
    }

    private Box createHeaderBlock(String url) {
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(url))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }


    private Box createBodyBlock(List<Menu> menuList) {
        List<FlexComponent> menuListBox = new ArrayList<FlexComponent>();

        for (Menu menu : menuList) {

            final Text menuText = Text.builder()
                    .text(String.format("%s", menu.getName()))
                    .weight(Text.TextWeight.BOLD)
                    .color("#000000")
                    .gravity(FlexGravity.CENTER)
                    .align(FlexAlign.START)
                    .size(FlexFontSize.Md)
                    .build();

            final Text menuPrice = Text.builder()
                    .text(String.format("Rp%s", menu.getPrice()))
                    .color("#000000")
                    .gravity(FlexGravity.CENTER)
                    .align(FlexAlign.END)
                    .size(FlexFontSize.Md)
                    .build();

            final PostbackAction addButtonPostback = PostbackAction.builder()
                    .label("Tambah")
                    .data(String.format(
                            "{\"name\":\"%s\",\"price\":%d,\"command\":\"%s\"}",
                            menu.getName(), menu.getPrice(), this.addCommand
                    ))
                    .displayText(
                            String.format("Tambahkan 1 %s ke dalam pesanan saya.", menu.getName()))
                    .build();

            final Button addButton = Button.builder()
                    .style(Button.ButtonStyle.PRIMARY)
                    .color("#65BF71")
                    .gravity(FlexGravity.CENTER)
                    .height(Button.ButtonHeight.SMALL)
                    .action(addButtonPostback)
                    .build();

            final PostbackAction deleteButtonPostback = PostbackAction.builder()
                    .label("Hapus")
                    .data(String.format(
                            "{\"name\":\"%s\",\"price\":%d,\"command\":\"%s\"}",
                            menu.getName(), menu.getPrice(), this.deleteCommand
                    ))
                    .displayText(
                            String.format(
                                    "Hapus 1 %s dari pesanan saya.", menu.getName()))
                    .build();

            final Button deleteButton = Button.builder()
                    .style(Button.ButtonStyle.SECONDARY)
                    .gravity(FlexGravity.CENTER)
                    .action(deleteButtonPostback)
                    .height(Button.ButtonHeight.SMALL)
                    .build();

            final Box menuDescriptionBox = Box.builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .contents(Arrays.asList(menuText, menuPrice))
                    .margin(FlexMarginSize.MD)
                    .spacing(FlexMarginSize.SM)
                    .build();

            final Box menuButtonBox  = Box.builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .contents(Arrays.asList(addButton, deleteButton))
                    .spacing(FlexMarginSize.MD)
                    .margin(FlexMarginSize.MD)
                    .build();

            final Box menuItemBox = Box.builder()
                    .layout(FlexLayout.VERTICAL)
                    .contents(Arrays.asList(menuDescriptionBox, menuButtonBox))
                    .borderColor("#CCCCCC")
                    .borderWidth(FlexBorderWidthSize.LIGHT)
                    .cornerRadius(FlexCornerRadiusSize.SM)
                    .paddingAll(FlexPaddingSize.LG)
                    .spacing(FlexMarginSize.MD)
                    .build();

            menuListBox.add(menuItemBox);
        }

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .margin(FlexMarginSize.NONE)
                .contents(menuListBox)
                .build();
    }

    private Box createFooterBlock() {
        String footerUrl = "https://i.ibb.co/jvsQ4gZ/wartegin-logo-3x1-1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(footerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }

}
