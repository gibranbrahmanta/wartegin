package com.herokuapp.wartegin.messages;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.flex.unit.FlexPaddingSize;
import java.net.URI;
import java.util.Arrays;
import java.util.function.Supplier;

public class NavigationFlexMessageSupplier implements Supplier<FlexMessage> {

    @Override
    public FlexMessage get() {

        final Box header = this.createHeaderBlock();

        final Image hero = createHeroBlock();

        final Box body = this.createBodyBlock();

        final Box footer = this.createFooterBlock();

        final Bubble bubble = Bubble.builder()
                .header(header)
                .hero(hero)
                .body(body)
                .footer(footer)
                .build();

        return new FlexMessage("Halo, selamat datang di WartegIn!", bubble);
    }

    private Box createHeaderBlock() {
        String headerUrl = "https://i.ibb.co/M2XN3sr/selamat-datang-3x1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(headerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }

    private Image createHeroBlock() {
        String heroUrl = "https://i.ibb.co/QpFxkmx/mbak-e-16x9.png";
        return Image.builder()
                .url(URI.create(heroUrl))
                .align(FlexAlign.CENTER)
                .gravity(FlexGravity.CENTER)
                .size(Image.ImageSize.FULL_WIDTH)
                .margin(FlexMarginSize.NONE)
                .aspectRatio(Image.ImageAspectRatio.R16TO9)
                .aspectMode(Image.ImageAspectMode.Cover)
                .build();
    }


    private Box createBodyBlock() {
        Text greeting = Text.builder()
                .text("Halo, selamat datang di WartegIn! Saya Mbak E!,"
                + " apakah ada yang bisa saya bantu? \n")
                .align(FlexAlign.CENTER)
                .gravity(FlexGravity.CENTER)
                .wrap(true)
                .color("#000000")
                .build();


        Button makeOrderButton = Button.builder()
                .action(
                        PostbackAction
                                .builder()
                                .label("Buat Pesanan")
                                .data("{\"command\":\"makeOrder\"}")
                                .displayText("Saya ingin buat pesanan.")
                                .build()
                )
                .color("#65BF71")
                .style(Button.ButtonStyle.PRIMARY)
                .gravity(FlexGravity.CENTER)
                .build();


        Button seePromo = Button.builder()
                .action(
                        PostbackAction
                                .builder()
                                .label("Lihat Promo Anda")
                                .data("{\"command\":\"seePromo\"}")
                                .displayText("Saya ingin lihat promo saya.")
                                .build()
                )
                .color("#65BF71")
                .style(Button.ButtonStyle.PRIMARY)
                .gravity(FlexGravity.CENTER)
                .build();


        Button aboutWartegin = Button.builder()
                .action(
                        PostbackAction
                                .builder()
                                .label("Tentang WartegIn!")
                                .data("{\"command\":\"aboutWartegin\"}")
                                .displayText("Apa itu WartegIn!?")
                                .build()
                )
                .color("#65BF71")
                .style(Button.ButtonStyle.PRIMARY)
                .gravity(FlexGravity.CENTER)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(greeting, makeOrderButton, seePromo, aboutWartegin))
                .margin(FlexMarginSize.NONE)
                .spacing(FlexMarginSize.MD)
                .build();

    }

    private Box createFooterBlock() {
        String footerUrl = "https://i.ibb.co/jvsQ4gZ/wartegin-logo-3x1-1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(footerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }


}

