package com.herokuapp.wartegin.messages;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.flex.unit.FlexPaddingSize;
import java.net.URI;
import java.util.Arrays;
import java.util.function.Supplier;

public class MenuNavigationFlexMessageSupplier implements Supplier<FlexMessage> {

    @Override
    public FlexMessage get() {

        Button showOrderList = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .gravity(FlexGravity.CENTER)
                .color("#65BF71")
                .action(
                        PostbackAction.builder()
                                .label("Lihat Pesanan Anda")
                                .data("{\"command\":\"showOrderList\"}")
                                .displayText("Lihat pesanan saya.")
                                .build()
                )
                .build();

        Button showPromoList = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .gravity(FlexGravity.CENTER)
                .color("#65BF71")
                .action(
                        PostbackAction.builder()
                                .label("Lihat Promo Anda")
                                .data("{\"command\":\"seePromo\"}")
                                .displayText("Lihat promo saya.")
                                .build()
                )
                .build();

        Button cancelOrderButton = Button.builder()
                .style(Button.ButtonStyle.SECONDARY)
                .gravity(FlexGravity.CENTER)
                .action(
                        PostbackAction.builder()
                                .label("Batalkan Pesanan")
                                .displayText("Batalkan pesanan saya.")
                                .data("{\"command\":\"cancelOrder\"}")
                                .build()
                )
                .build();

        Box body = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Arrays.asList(showOrderList, showPromoList, cancelOrderButton))
                .spacing(FlexMarginSize.MD)
                .build();

        final Box footer = createFooterBlock();

        Bubble bubble = Bubble.builder()
                .body(body)
                .footer(footer)
                .build();

        return new FlexMessage("Apakah yang ingin Anda lakukan?", bubble);
    }



    private Box createFooterBlock() {
        String footerUrl = "https://i.ibb.co/jvsQ4gZ/wartegin-logo-3x1-1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(footerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }
}
