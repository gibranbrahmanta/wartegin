package com.herokuapp.wartegin.messages;

import com.herokuapp.wartegin.model.Promo;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.flex.unit.FlexPaddingSize;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class PromoListFlexMessageSupplier implements Supplier<FlexMessage> {

    private final List<Promo> promoList;

    public PromoListFlexMessageSupplier(List<Promo> promoList) {
        this.promoList = promoList;
    }

    @Override
    public FlexMessage get() {

        final Box header = this.createHeaderBlock();

        final Box body = this.createBodyBlock(this.promoList);

        final Box footer = createFooterBlock();

        final Bubble bubble = Bubble.builder()
                .header(header)
                .body(body)
                .footer(footer)
                .build();

        return new FlexMessage("Berikut ini adalah promo yang Anda miliki.", bubble);
    }

    private Box createHeaderBlock() {
        String headerUrl = "https://i.ibb.co/vzT0Mj4/promo-anda-3x1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(headerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }


    private Box createBodyBlock(List<Promo> promoList) {
        List<FlexComponent> promoListBox = new ArrayList<FlexComponent>();

        for (Promo promo : promoList) {

            final Text promoTitle = Text.builder()
                    .text(String.format("%s", promo.getName()))
                    .weight(Text.TextWeight.BOLD)
                    .color("#000000")
                    .gravity(FlexGravity.CENTER)
                    .align(FlexAlign.START)
                    .size(FlexFontSize.Md)
                    .wrap(true)
                    .build();

            final Text promoDescription = Text.builder()
                    .text(String.format("%s", promo.getDescription()))
                    .color("#000000")
                    .gravity(FlexGravity.CENTER)
                    .align(FlexAlign.START)
                    .size(FlexFontSize.SM)
                    .wrap(true)
                    .build();

            final Text promoCount = Text.builder()
                    .text(String.format("%s x\n", promo.getCount()))
                    .weight(Text.TextWeight.BOLD)
                    .color("#65BF71")
                    .gravity(FlexGravity.CENTER)
                    .align(FlexAlign.START)
                    .size(FlexFontSize.LG)
                    .wrap(true)
                    .build();

            final Box promoDescriptionBox = Box.builder()
                    .layout(FlexLayout.HORIZONTAL)
                    .contents(Arrays.asList(promoTitle, promoDescription))
                    .build();

            final Box promoBox = Box.builder()
                    .layout(FlexLayout.VERTICAL)
                    .contents(Arrays.asList(promoDescriptionBox, promoCount))
                    .build();

            promoListBox.add(promoBox);
        }

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(promoListBox)
                .margin(FlexMarginSize.NONE)
                .build();
    }

    private Box createFooterBlock() {
        String footerUrl = "https://i.ibb.co/jvsQ4gZ/wartegin-logo-3x1-1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(footerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }

}
