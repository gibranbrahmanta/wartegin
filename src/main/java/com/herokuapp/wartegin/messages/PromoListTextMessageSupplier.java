package com.herokuapp.wartegin.messages;

import com.linecorp.bot.model.message.TextMessage;
import java.util.function.Supplier;

public class PromoListTextMessageSupplier implements Supplier<TextMessage> {
    private final String promoList;

    public PromoListTextMessageSupplier(String promoList) {
        this.promoList = promoList;
    }

    @Override
    public TextMessage get() {
        return TextMessage.builder()
                .text(promoList)
                .build();
    }
}


