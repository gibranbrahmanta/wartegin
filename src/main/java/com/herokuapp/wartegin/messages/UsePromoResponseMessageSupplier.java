package com.herokuapp.wartegin.messages;

import com.linecorp.bot.model.message.TextMessage;
import java.util.function.Supplier;

public class UsePromoResponseMessageSupplier implements Supplier<TextMessage> {

    private final String promoName;
    private final String command;

    public UsePromoResponseMessageSupplier(String promoName, String command) {
        this.promoName = promoName;
        this.command = command;
    }

    @Override
    public TextMessage get() {

        String replyText = String.format(
                "%s berhasil %s ! \n" + "(Tekan 'Ya' untuk menggunakan promo yang Anda pilih"
                + "atau tekan 'Tidak' untuk membatalkan promo)",
                this.promoName, this.command
        );

        return TextMessage.builder()
                .text(replyText)
                .build();
    }
}
