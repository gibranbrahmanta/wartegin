package com.herokuapp.wartegin.messages;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.model.Driver;
import com.herokuapp.wartegin.model.Promo;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.flex.unit.FlexPaddingSize;
import java.net.URI;
import java.util.Arrays;
import java.util.function.Supplier;

public class OrderPromoDeliveryFlexMessageSupplier implements Supplier<FlexMessage> {

    private final Menu menu;
    private final Promo promo;
    private final Driver driver;

    public OrderPromoDeliveryFlexMessageSupplier(Menu menu, Promo promo, Driver driver) {
        this.menu = menu;
        this.promo = promo;
        this.driver = driver;
    }

    @Override
    public FlexMessage get() {

        final Box header = this.createHeaderBlock();

        final Box body = this.createBodyBlock();

        final Box footer = createFooterBlock();

        final Bubble bubble = Bubble.builder()
                .header(header)
                .body(body)
                .footer(footer)
                .build();

        return new FlexMessage("Berikut ini adalah pesanan Anda.", bubble);
    }

    private Box createHeaderBlock() {
        String headerUrl = "https://i.ibb.co/YTvx1fD/pesanan-anda-3x1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(headerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }


    private Box createBodyBlock() {
        final Text orderTitle = Text.builder()
                .text(String.format("Pesanan Anda:"))
                .weight(Text.TextWeight.BOLD)
                .color("#65BF71")
                .gravity(FlexGravity.CENTER)
                .align(FlexAlign.START)
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        final Text order = Text.builder()
                .text(String.format("%s", this.menu.getDescription()))
                .color("#000000")
                .gravity(FlexGravity.CENTER)
                .align(FlexAlign.START)
                .size(FlexFontSize.SM)
                .wrap(true)
                .build();

        final Text totalPrice = Text.builder()
                .text(String.format("Harga Total: Rp%s \n", this.menu.getPrice()))
                .weight(Text.TextWeight.BOLD)
                .color("#000000")
                .gravity(FlexGravity.CENTER)
                .align(FlexAlign.START)
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();


        final Text checkoutPrice = Text.builder()
                .text(String.format("Harga Akhir: Rp%s \n", (int) getTotalPrice()))
                .weight(Text.TextWeight.BOLD)
                .color("#65BF71")
                .gravity(FlexGravity.CENTER)
                .align(FlexAlign.START)
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        final Text driver = Text.builder()
                .text(String.format("Pesanan diantar oleh %s %s", this.driver.getName(),
                        this.driver.getPlateNumber()))
                .weight(Text.TextWeight.BOLD)
                .color("#65BF71")
                .gravity(FlexGravity.CENTER)
                .align(FlexAlign.START)
                .size(FlexFontSize.Md)
                .wrap(true)
                .build();

        final Box orderBox;

        if (this.promo != null) {
            final Text promo = Text.builder()
                    .text(String.format("Promo %s", this.promo.getName()))
                    .weight(Text.TextWeight.BOLD)
                    .color("#65BF71")
                    .gravity(FlexGravity.CENTER)
                    .align(FlexAlign.START)
                    .size(FlexFontSize.Md)
                    .wrap(true)
                    .build();

            orderBox = Box.builder()
                    .layout(FlexLayout.VERTICAL)
                    .contents(
                            Arrays.asList(orderTitle, order, totalPrice,
                                    promo, checkoutPrice, driver))
                    .build();
        } else {
            orderBox = Box.builder()
                    .layout(FlexLayout.VERTICAL)
                    .contents(Arrays.asList(orderTitle, order, totalPrice, checkoutPrice, driver))
                    .build();
        }

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(orderBox)
                .margin(FlexMarginSize.NONE)
                .build();

    }

    private Box createFooterBlock() {
        String footerUrl = "https://i.ibb.co/jvsQ4gZ/wartegin-logo-3x1-1.png";
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(Image.builder()
                        .url(URI.create(footerUrl))
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .margin(FlexMarginSize.NONE)
                        .aspectRatio(Image.ImageAspectRatio.R3TO1)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build()
                )
                .paddingAll(FlexPaddingSize.NONE)
                .margin(FlexMarginSize.NONE)
                .build();
    }

    private double getTotalPrice() {
        double totalPrice = this.menu.getPrice();

        if (this.promo != null) {
            totalPrice = (totalPrice - this.promo.getAmount()) * this.promo.getPercent();
        }

        if (totalPrice <= 0) {
            totalPrice = 0;
        }

        return totalPrice;
    }
}
