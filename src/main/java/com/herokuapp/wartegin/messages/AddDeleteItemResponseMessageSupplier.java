package com.herokuapp.wartegin.messages;

import com.herokuapp.wartegin.composite.Menu;
import com.linecorp.bot.model.message.TextMessage;
import java.util.function.Supplier;

public class AddDeleteItemResponseMessageSupplier implements Supplier<TextMessage> {

    private final String item;
    private final String command;
    private final Menu menu;

    public AddDeleteItemResponseMessageSupplier(String item, String command, Menu menu) {
        this.item = item;
        this.command = command;
        this.menu = menu;
    }

    @Override
    public TextMessage get() {
        String replyText = String.format(
                "%s berhasil %s! \nJumlah item menu dalam pesanan Anda: %d \n"
                        + "(Tekan 'Ya' untuk mengkonfirmasi pesanan Anda "
                        + "atau tekan 'Tidak' untuk membatalkan pesanan Anda)",
                this.item, this.command, this.menu.getTotalMenuComponents()
        );
        return TextMessage.builder()
                .text(replyText)
                .build();
    }
}
