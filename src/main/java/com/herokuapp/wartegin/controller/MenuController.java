package com.herokuapp.wartegin.controller;

import com.herokuapp.wartegin.model.Menu;
import com.herokuapp.wartegin.service.MenuService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;


    @GetMapping
    public ResponseEntity<List<Menu>> findAll() {
        return ResponseEntity.ok(menuService.findAll());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Menu> findMenu(@PathVariable long id) {
        if (menuService.findMenu(id).isPresent()) {
            Menu menu = menuService.findMenu(id).get();
            return ResponseEntity.ok(menu);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Menu> create(@RequestBody Menu menu) {
        if (menuService.isValid(menu)) {
            return ResponseEntity.ok(menuService.register(menu));
        }
        return ResponseEntity.badRequest().build();
    }

    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Menu> update(@PathVariable long id, @RequestBody Menu menu) {
        if ((menuService.isValid(menu)) && (menu.getId() == id)) {
            return ResponseEntity.ok(menuService.rewrite(menu));
        }
        return ResponseEntity.badRequest().build();

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        menuService.erase(id);
        return ResponseEntity.ok("Deleted");
    }
}
