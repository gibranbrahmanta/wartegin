package com.herokuapp.wartegin.controller;

import static java.util.Collections.singletonList;

import com.herokuapp.wartegin.composite.MenuComponent;
import com.herokuapp.wartegin.composite.MenuItem;
import com.herokuapp.wartegin.messages.AboutWarteginFlexMessageSupplier;
import com.herokuapp.wartegin.messages.NavigationFlexMessageSupplier;
import com.herokuapp.wartegin.model.LineUser;
import com.herokuapp.wartegin.service.DriverService;
import com.herokuapp.wartegin.service.LineUserService;
import com.herokuapp.wartegin.service.MenuService;
import com.herokuapp.wartegin.state.OrderState;
import com.herokuapp.wartegin.state.helper.OrderStateHelper;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;

@LineMessageHandler
public class LineMessageController {

    @Autowired
    private OrderStateHelper orderStateHelper;
    @Value("${line.bot.channel-token}")
    private String channelAccessToken;
    @Autowired
    private LineUserService lineUserService;
    @Autowired
    private MenuService menuService;
    @Autowired
    private DriverService driverService;
    @Autowired
    private LineMessagingClient lineMessagingClient;

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent)
            throws ExecutionException, InterruptedException {

        TextMessageContent msgContent = messageEvent.getMessage();
        Source source = messageEvent.getSource();
        String lineId = source.getUserId();
        String userDisplayName = getUserDisplayName(lineId);
        LineUser lineUser = findOrSaveUser(userDisplayName, lineId);
        String command = msgContent.getText().toLowerCase().trim();
        String replyToken = messageEvent.getReplyToken();

        switch (command.toLowerCase()) {
            case "mbake!":
                reply(replyToken, new NavigationFlexMessageSupplier().get());
                break;
            default:
                reply(replyToken,
                        TextMessage.builder()
                                .text("Untuk menghubungi MbakE!, silakan ketik 'MbakE!'.")
                                .build());
                break;
        }
    }

    @EventMapping
    public void handlePostbackEvent(PostbackEvent event) {
        List<Message> responses = new ArrayList<>();
        String replyToken = event.getReplyToken();
        Source source = event.getSource();

        String lineId = source.getUserId();
        OrderState state = orderStateHelper.getUserStateByLineId(lineId);

        String dataString = event.getPostbackContent().getData();
        JSONObject dataJSON = new JSONObject(dataString);
        String command = dataJSON.getString("command");
        MenuComponent menuComponent;
        String menuName;
        TextMessage replyMessage;

        switch (command) {
            case "addItem":
                menuName = dataJSON.getString("name");
                int menuPrice = dataJSON.getInt("price");
                menuComponent = new MenuItem(menuName, menuPrice);
                responses = state.addItem(menuComponent, lineId);
                break;
            case "deleteItem":
                menuName = dataJSON.getString("name");
                responses = state.deleteItem(menuName, lineId);
                break;
            case "finishOrder":
                responses = state.finishOrder(lineId);
                break;
            case "confirmOrder":
                responses = state.confirmOrder(lineId);
                break;
            case "cancelOrder":
                responses = state.cancelOrder(lineId);
                break;
            case "makeOrder":
                responses = state.makeOrder(lineId);
                break;
            case "continueOrder":
                replyMessage = TextMessage.builder()
                        .text("Silakan melanjutkan pemesanan Anda.")
                        .build();
                responses.add(replyMessage);
                break;
            case "notArrived":
                replyMessage = TextMessage.builder()
                        .text(
                                "Mohon tunggu pesanan Anda diantar oleh driver kami."
                                    + " Apabila pesanan sudah sampai, mohon mengkonfirmasi.")
                        .build();
                responses.add(replyMessage);
                break;
            case "showOrderList":
                responses = state.showOrderList(lineId);
                break;
            case "seePromo":
                responses = state.seePromo(lineId);
                break;
            case "usePromo":
                String usePromoName = dataJSON.getString("name");
                responses = state.usePromo(lineId, usePromoName);
                break;
            case "usedPromo":
                responses = state.usedPromo(lineId);
                break;
            case "cancelPromo":
                responses = state.cancelPromo(lineId);
                break;
            case "aboutWartegin":
                responses.add(new AboutWarteginFlexMessageSupplier().get());
                break;
            default:
                responses.add(new TextMessage("No command receive"));
        }

        reply(replyToken, responses);

    }

    private LineUser findOrSaveUser(String name, String lineId) {
        LineUser lineUser;

        if (!lineUserService.findUserById(lineId).isPresent()) {
            lineUser = new LineUser(name, lineId, 1);
            lineUserService.register(lineUser);
        }
        lineUser = lineUserService.findUserById(lineId).get();
        return lineUser;

    }

    public String getUserDisplayName(String userId)
            throws ExecutionException, InterruptedException {
        return lineMessagingClient.getProfile(userId).get().getDisplayName();
    }

    private void reply(@NonNull String replyToken, @NonNull Message message) {
        reply(replyToken, singletonList(message));
    }

    private void reply(@NonNull String replyToken, @NonNull List<Message> messages) {
        reply(replyToken, messages, false);
    }

    private void reply(@NonNull String replyToken,
                       @NonNull List<Message> messages,
                       boolean notificationDisabled) {
        try {
            BotApiResponse apiResponse = lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, messages, notificationDisabled))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

}
