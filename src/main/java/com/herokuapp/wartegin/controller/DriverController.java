package com.herokuapp.wartegin.controller;

import com.herokuapp.wartegin.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DriverController {

    @Autowired
    private DriverService driverService;

    @RequestMapping(value = "/add-driver", method = RequestMethod.GET)
    public String createRequest(Model model) {
        model.addAttribute("drivers", driverService.getDrivers());
        return "add-driver";
    }

}
