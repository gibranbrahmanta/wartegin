package com.herokuapp.wartegin.controller;

import com.herokuapp.wartegin.service.PromoServiceImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PromoController {
    @Autowired
    private PromoServiceImplementation promoServiceImplementation;

    @RequestMapping(value = "/add-promo", method = RequestMethod.GET)
    public String createRequest(Model model) {
        model.addAttribute("promos", promoServiceImplementation.getPromosList());
        return "add-promo";
    }
}
