package com.herokuapp.wartegin.controller;

import com.herokuapp.wartegin.model.LineUser;
import com.herokuapp.wartegin.service.LineUserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/user")
public class LineUserController {

    @Autowired
    private LineUserService lineUserService;

    @GetMapping
    public ResponseEntity<List<LineUser>> findAll() {
        return ResponseEntity.ok(lineUserService.findAll());
    }

    @GetMapping(value = "/{lineId}")
    public ResponseEntity<LineUser> findUserByLineId(@PathVariable String lineId) {

        if (lineUserService.findUserById(lineId).isPresent()) {
            LineUser lineUser = lineUserService.findUserById(lineId).get();
            return ResponseEntity.ok(lineUser);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LineUser> create(@RequestBody LineUser lineUser) {
        if (lineUserService.isValid(lineUser)) {

            return ResponseEntity.ok(lineUserService.register(lineUser));
        }
        return ResponseEntity.badRequest().build();
    }

    @PutMapping(value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LineUser> update(@PathVariable String id,
                                           @RequestBody LineUser lineUser) {

        if (lineUserService.isValid(lineUser) && lineUser.getLineId().equals(id)) {
            return ResponseEntity.ok(lineUserService.rewrite(lineUser));
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable String id) {
        lineUserService.erase(id);
        return ResponseEntity.ok("Deleted");
    }
}
