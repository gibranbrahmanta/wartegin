package com.herokuapp.wartegin.repository;

import com.herokuapp.wartegin.model.Driver;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class DriverRepository {

    private final Map<String, Driver> drivers = new HashMap<>();
    private final Map<String, Driver> vacantDrivers = new HashMap<>();

    public List<Driver> getDriversList() {
        return new ArrayList<Driver>(drivers.values());
    }

    public Map<String, Driver> getVacantDriversMap() {
        return vacantDrivers;
    }

    public ArrayList<Driver> getVacantDriversList() {
        return new ArrayList<>(vacantDrivers.values());
    }

    public Driver addVacantDriver(Driver driver) {
        String plateNumber = driver.getPlateNumber();
        if (vacantDrivers.get(plateNumber) == null) {
            vacantDrivers.put(plateNumber, driver);
            return driver;
        } else {
            return vacantDrivers.get(plateNumber);
        }
    }

    public void addDriver(Driver driver) {
        String plateNumber = driver.getPlateNumber();
        drivers.putIfAbsent(plateNumber, driver);
    }

    public void addDrivers(ArrayList<Driver> driversArrayList) {
        for (Driver driver : driversArrayList) {
            addDriver(driver);
        }
    }

    public void addVacantDrivers(ArrayList<Driver> driversArrayList) {
        for (Driver driver : driversArrayList) {
            addVacantDriver(driver);
        }
    }
}
