package com.herokuapp.wartegin.repository;

import com.herokuapp.wartegin.model.Promo;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.stereotype.Repository;


@Repository
public class PromoRepository {
    private final HashMap<String, Promo> promos = new HashMap<>();

    public HashMap<String, Promo> getPromos() {
        return promos;
    }


    public void addPromo(Promo promo) {
        String id = promo.getName();
        if (promos.get(id) == null) {
            promos.put(id, promo);
        } else {
            Promo setPromo = promos.get(id);
            setPromo.addCount(promo.getCount());
        }
    }

    public void addPromos(ArrayList<Promo> promosArrayList) {
        for (Promo promo : promosArrayList) {
            addPromo(promo);
        }
    }

}
