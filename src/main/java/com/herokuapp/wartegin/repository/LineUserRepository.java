package com.herokuapp.wartegin.repository;

import com.herokuapp.wartegin.model.LineUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LineUserRepository extends JpaRepository<LineUser, String> {

    @Query("SELECT u FROM LineUser u WHERE u.lineId = ?1")
    LineUser findByLineId(String lineId);
}
