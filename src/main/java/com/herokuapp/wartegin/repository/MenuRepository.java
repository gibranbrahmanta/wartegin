package com.herokuapp.wartegin.repository;

import com.herokuapp.wartegin.model.Menu;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MenuRepository extends JpaRepository<Menu, Long> {

    @Query(value = "SELECT m FROM Menu m WHERE m.type = 1")
    List<Menu> findAllFood();

    @Query(value = "SELECT m FROM Menu m WHERE m.type = 2")
    List<Menu> findAllDrink();

}
