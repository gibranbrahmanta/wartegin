package com.herokuapp.wartegin.state;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.composite.MenuComponent;
import com.herokuapp.wartegin.messages.AddDeleteItemResponseMessageSupplier;
import com.herokuapp.wartegin.messages.ConfirmTemplateFlexMessageSupplier;
import com.herokuapp.wartegin.messages.OrderListFlexMessageSupplier;
import com.herokuapp.wartegin.messages.PromoListFlexMessageSupplier;
import com.herokuapp.wartegin.messages.PromoUseFlexMessageSupplier;
import com.herokuapp.wartegin.messages.state.MakeOrderStateMessages;
import com.linecorp.bot.model.message.Message;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MakeOrderState extends OrderState {

    public static final String STATE_NAME = "MAKE_ORDER_STATE";

    @Autowired
    private MakeOrderStateMessages makeOrderStateMessages;

    @Override
    public List<Message> addItem(MenuComponent item, String lineId) {
        this.clearResponses();
        this.orderStateHelper.getUserMenuByLineId(lineId).add(item);
        Menu currentMenu = this.orderStateHelper.getUserMenuByLineId(lineId);
        this.responses.add(
                new AddDeleteItemResponseMessageSupplier(item.getName(),
                        "ditambahkan", currentMenu).get());
        return responses;
    }

    @Override
    public List<Message> deleteItem(String item, String lineId) {
        this.clearResponses();
        this.orderStateHelper.getUserMenuByLineId(lineId).remove(item);
        this.responses.add(this.makeOrderStateMessages.getDeleteItemMessage());
        Menu currentMenu = this.orderStateHelper.getUserMenuByLineId(lineId);
        this.responses
                .add(new AddDeleteItemResponseMessageSupplier(item,
                        "dihapus", currentMenu).get());
        return responses;
    }

    @Override
    public List<Message> makeOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.makeOrderStateMessages.getMakeOrderMessage());
        return responses;
    }

    @Override
    public List<Message> confirmOrder(String lineId) {
        this.clearResponses();
        OrderState usePromoState =
                this.orderStateHelper.getStateByStateName(UsePromoState.STATE_NAME);
        this.orderStateHelper.setUserStateByLineId(lineId, usePromoState);

        this.responses.add(this.makeOrderStateMessages.getUsePromoMessage());
        this.responses.add(
                new PromoUseFlexMessageSupplier(
                        this.orderStateHelper.getPromosByLineId(lineId)).get()
        );
        this.responses.add(
                new ConfirmTemplateFlexMessageSupplier(
                        "Apakah Anda ingin menggunakan promo untuk pesanan Anda?", "usedPromo",
                        "Promo saya gunakan.", "cancelPromo", "Promo tidak saya gunakan.").get()
        );
        return responses;
    }

    @Override
    public List<Message> cancelOrder(String lineId) {
        this.clearResponses();
        OrderState idleOrderState =
                this.orderStateHelper.getStateByStateName(IdleOrderState.STATE_NAME);
        this.orderStateHelper.setUserStateByLineId(lineId, idleOrderState);
        this.orderStateHelper.setUserMenuByLineId(lineId, null);
        this.responses.add(this.makeOrderStateMessages.getCancelOrderMessage());
        return responses;
    }

    @Override
    public List<Message> finishOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.makeOrderStateMessages.getFinishOrderMessage());
        return responses;
    }

    @Override
    public List<Message> showOrderList(String lineId) {
        this.clearResponses();
        Menu orderedMenu = this.orderStateHelper.getUserMenuByLineId(lineId);
        this.responses.add(
                new OrderListFlexMessageSupplier(orderedMenu).get()
        );
        return responses;
    }

    @Override
    public List<Message> seePromo(String lineId) {
        this.clearResponses();
        this.responses.add(
                new PromoListFlexMessageSupplier(this.orderStateHelper.getUserPromos(lineId)).get()
        );
        return responses;
    }

    @Override
    public List<Message> usedPromo(String lineId) {
        this.clearResponses();
        this.responses.add(this.makeOrderStateMessages.getUsedPromoMessage());
        return responses;
    }

    @Override
    public List<Message> usePromo(String lineId, String name) {
        this.clearResponses();
        this.responses.add(this.makeOrderStateMessages.getUsePromoMessage());
        return responses;
    }

    @Override
    public List<Message> cancelPromo(String lineId) {
        this.clearResponses();
        this.responses.add(this.makeOrderStateMessages.getCancelPromoMessage());
        return responses;
    }
}
