package com.herokuapp.wartegin.state;

import com.herokuapp.wartegin.composite.MenuComponent;
import com.herokuapp.wartegin.service.MenuService;
import com.herokuapp.wartegin.state.helper.OrderStateHelper;
import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class OrderState {

    @Autowired
    protected MenuService menuService;

    @Autowired
    protected OrderStateHelper orderStateHelper;

    protected ArrayList<Message> responses = new ArrayList<>();

    public abstract List<Message> addItem(MenuComponent item, String lineId);

    public abstract List<Message> deleteItem(String item, String lineId);

    public abstract List<Message> makeOrder(String lineId);

    public abstract List<Message> confirmOrder(String lineId);

    public abstract List<Message> cancelOrder(String lineId);

    public abstract List<Message> finishOrder(String lineId);

    public abstract List<Message> showOrderList(String lineId);

    public abstract List<Message> seePromo(String lineId);

    public abstract List<Message> cancelPromo(String lineId);

    public void clearResponses() {
        this.responses.clear();
    }

    public abstract List<Message> usePromo(String lineId, String promoName);

    public abstract List<Message> usedPromo(String lineId);
}
