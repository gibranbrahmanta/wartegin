package com.herokuapp.wartegin.state;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.composite.MenuComponent;
import com.herokuapp.wartegin.messages.OrderListFlexMessageSupplier;
import com.herokuapp.wartegin.messages.PromoListFlexMessageSupplier;
import com.herokuapp.wartegin.messages.PromoListTextMessageSupplier;
import com.herokuapp.wartegin.messages.state.ProcessOrderStateMessages;
import com.herokuapp.wartegin.model.Driver;
import com.linecorp.bot.model.message.Message;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class ProcessOrderState extends OrderState {

    public static final String STATE_NAME = "PROCESS_ORDER_STATE";

    @Autowired
    private ProcessOrderStateMessages processOrderStateMessages;

    @Override
    public List<Message> addItem(MenuComponent item, String lineId) {
        this.clearResponses();
        this.responses.add(this.processOrderStateMessages.getAddItemMessage());
        return responses;
    }

    @Override
    public List<Message> deleteItem(String item, String lineId) {
        this.clearResponses();
        this.responses.add(this.processOrderStateMessages.getDeleteItemMessage());
        return responses;
    }

    @Override
    public List<Message> makeOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.processOrderStateMessages.getMakeOrderMessage());
        return responses;
    }

    @Override
    public List<Message> confirmOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.processOrderStateMessages.getConfirmOrderMessage());
        return responses;
    }

    @Override
    public List<Message> cancelOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.processOrderStateMessages.getCancelOrderMessage());
        return responses;
    }

    @Override
    public List<Message> finishOrder(String lineId) {
        this.clearResponses();

        Driver driver = this.orderStateHelper.getUserDriverByLineId(lineId);
        this.orderStateHelper.vacantUserDriver(driver);
        this.orderStateHelper.removeUserDriverByLineId(lineId);

        OrderState idleOrderState =
                this.orderStateHelper.getStateByStateName(IdleOrderState.STATE_NAME);
        this.orderStateHelper.setUserStateByLineId(lineId, idleOrderState);

        this.responses.add(this.processOrderStateMessages.getFinishOrderMessage());
        return responses;
    }

    @Override
    public List<Message> showOrderList(String lineId) {
        this.clearResponses();
        Menu orderedMenu = this.orderStateHelper.getUserMenuByLineId(lineId);
        this.responses.add(
                new OrderListFlexMessageSupplier(orderedMenu).get()
        );
        return responses;
    }

    @Override
    public List<Message> seePromo(String lineId) {
        this.clearResponses();
        this.responses.add(
                new PromoListFlexMessageSupplier(this.orderStateHelper.getUserPromos(lineId)).get()
        );
        return responses;
    }

    @Override
    public List<Message> usedPromo(String lineId) {
        this.clearResponses();
        this.responses.add(
                new PromoListTextMessageSupplier(this.orderStateHelper.getStringAllPromo()).get()
        );
        return responses;
    }

    @Override
    public List<Message> usePromo(String lineId, String name) {
        this.clearResponses();
        this.responses.add(this.processOrderStateMessages.getUsedPromoMessage());
        return responses;
    }

    @Override
    public List<Message> cancelPromo(String lineId) {
        this.clearResponses();
        this.responses.add(this.processOrderStateMessages.getCancelPromoMessage());
        return responses;
    }

}
