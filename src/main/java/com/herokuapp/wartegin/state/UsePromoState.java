package com.herokuapp.wartegin.state;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.composite.MenuComponent;
import com.herokuapp.wartegin.messages.ConfirmTemplateFlexMessageSupplier;
import com.herokuapp.wartegin.messages.OrderListFlexMessageSupplier;
import com.herokuapp.wartegin.messages.OrderPromoDeliveryFlexMessageSupplier;
import com.herokuapp.wartegin.messages.PromoListFlexMessageSupplier;
import com.herokuapp.wartegin.messages.UsePromoResponseMessageSupplier;
import com.herokuapp.wartegin.messages.state.PromoOrderStateMessages;
import com.herokuapp.wartegin.model.Driver;
import com.herokuapp.wartegin.model.Promo;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class UsePromoState extends OrderState {

    public static final String STATE_NAME = "USE_PROMO_STATE";

    @Autowired
    private PromoOrderStateMessages promoOrderStateMessages;

    @Override
    public List<Message> addItem(MenuComponent item, String lineId) {
        this.clearResponses();
        this.responses.add(this.promoOrderStateMessages.getAddItemMessage());
        return responses;
    }

    @Override
    public List<Message> deleteItem(String item, String lineId) {
        this.clearResponses();
        this.responses.add(this.promoOrderStateMessages.getDeleteItemMessage());
        return responses;
    }

    @Override
    public List<Message> makeOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.promoOrderStateMessages.getMakeOrderMessage());
        return responses;
    }

    @Override
    public List<Message> confirmOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.promoOrderStateMessages.getConfirmOrderMessage());
        return responses;
    }

    @Override
    public List<Message> cancelOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.promoOrderStateMessages.getCancelOrderMessage());
        return responses;
    }

    @Override
    public List<Message> finishOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.promoOrderStateMessages.getFinishOrderMessage());
        return responses;
    }

    @Override
    public List<Message> showOrderList(String lineId) {
        this.clearResponses();
        Menu orderedMenu = this.orderStateHelper.getUserMenuByLineId(lineId);
        this.responses.add(
                new OrderListFlexMessageSupplier(orderedMenu).get()
        );
        return responses;
    }

    @Override
    public List<Message> seePromo(String lineId) {
        this.clearResponses();
        this.responses.add(
                new PromoListFlexMessageSupplier(this.orderStateHelper.getUserPromos(lineId)).get()
        );
        return responses;
    }

    @Override
    public List<Message> usedPromo(String lineId) {
        this.clearResponses();
        Promo usedPromo = this.orderStateHelper.getUsedPromo(lineId);
        if (usedPromo != null) {
            this.orderStateHelper.useUserPromo(lineId, usedPromo.getName());
            this.responses.add(promoOrderStateMessages.getUsedPromoMessage());
            return goToProcessOrderState(lineId, usedPromo);
        } else {
            this.responses.add(new TextMessage("Anda harus menambahkan promo terlebih dahulu."));
            return responses;
        }
    }

    @Override
    public List<Message> usePromo(String lineId, String promoName) {
        this.clearResponses();
        Promo promo = this.orderStateHelper.usedPromo(lineId, promoName);
        this.responses.add(new UsePromoResponseMessageSupplier(promo.getName(), "digunakan").get());
        return responses;
    }

    @Override
    public List<Message> cancelPromo(String lineId) {
        this.clearResponses();
        this.orderStateHelper.cancelPromo(lineId);
        Promo usedPromo = this.orderStateHelper.getUsedPromo(lineId);
        this.responses.add(promoOrderStateMessages.getCancelPromoMessage());
        return goToProcessOrderState(lineId, usedPromo);
    }


    public List<Message> goToProcessOrderState(String lineId, Promo usedPromo) {
        OrderState processOrderState =
                this.orderStateHelper.getStateByStateName(ProcessOrderState.STATE_NAME);
        this.orderStateHelper.setUserStateByLineId(lineId, processOrderState);

        Menu orderedMenu = this.orderStateHelper.getUserMenuByLineId(lineId);

        Driver driver = this.orderStateHelper.getUserDriver();

        this.orderStateHelper.setUserDriverByLineId(lineId, driver);

        this.responses.add(
                new OrderPromoDeliveryFlexMessageSupplier(
                        orderedMenu, usedPromo, driver
                ).get()
        );

        this.responses.add(
                new ConfirmTemplateFlexMessageSupplier(
                        "Apakah pesanan Anda sudah sampai?", "finishOrder",
                        "Pesanan sudah saya terima. Terimakasih WartegIn!", "notArrived",
                        "Pesanan belum saya terima.").get()
        );
        return responses;
    }

}
