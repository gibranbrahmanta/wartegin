package com.herokuapp.wartegin.state.helper;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.model.Driver;
import com.herokuapp.wartegin.model.Promo;
import com.herokuapp.wartegin.service.DriverService;
import com.herokuapp.wartegin.service.PromoService;
import com.herokuapp.wartegin.state.IdleOrderState;
import com.herokuapp.wartegin.state.MakeOrderState;
import com.herokuapp.wartegin.state.OrderState;
import com.herokuapp.wartegin.state.ProcessOrderState;
import com.herokuapp.wartegin.state.UsePromoState;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class OrderStateHelper {

    private HashMap<String, OrderState> usersState = new HashMap<>();
    private HashMap<String, Menu> usersOrderedMenu = new HashMap<>();
    private HashMap<String, HashMap<String, Promo>> usersPromo = new HashMap<>();
    private HashMap<String, Promo> usersUsedPromo = new HashMap<>();
    private HashMap<String, Driver> usersDriver = new HashMap<>();
    @Autowired
    private IdleOrderState idleOrderState;
    @Autowired
    private MakeOrderState makeOrderState;
    @Autowired
    private UsePromoState usePromoState;
    @Autowired
    private ProcessOrderState processOrderState;
    @Autowired
    private DriverService driverService;
    @Autowired
    private PromoService promoService;

    public OrderState getUserStateByLineId(String lineId) {
        if (usersState.get(lineId) == null) {
            setUserStateByLineId(lineId, this.idleOrderState);
        }
        return usersState.get(lineId);
    }

    public void setUserStateByLineId(String lineId, OrderState orderState) {
        this.usersState.put(lineId, orderState);
    }

    public Menu getUserMenuByLineId(String lineId) {
        return this.usersOrderedMenu.get(lineId);
    }

    public void setUserMenuByLineId(String lineId, Menu orderedMenu) {
        this.usersOrderedMenu.put(lineId, orderedMenu);
    }

    public Driver getUserDriverByLineId(String lineId) {
        return this.usersDriver.get(lineId);
    }

    public void setUserDriverByLineId(String lineId, Driver driver) {
        this.usersDriver.put(lineId, driver);
    }

    public void removeUserDriverByLineId(String lineId) {
        this.usersDriver.remove(lineId);
    }

    public Driver getUserDriver() {
        Driver driver = driverService.getRandomVacantDriver();
        this.driverService.occupyDriver(driver.getName());
        return driver;
    }

    public boolean driverVacancy() {
        return this.driverService.getVacantDriversList().size() != 0;
    }

    public void vacantUserDriver(Driver driver) {
        this.driverService.vacantDriver(driver);
    }

    public String getStringAllPromo() {
        return this.promoService.getPromosString();
    }

    public ArrayList<Promo> getUserPromos(String lineId) {
        return new ArrayList<Promo>(this.usersPromo.get(lineId).values());
    }

    public Promo getPromoFromUserPromo(String lineId, String id) {
        return this.usersPromo.get(lineId).get(id);
    }

    public ArrayList<Promo> getPromosByLineId(String lineId) {
        return new ArrayList<>(this.usersPromo.get(lineId).values());
    }

    public void setInitialPromosByLineId(String lineId) {
        if (usersPromo.get(lineId) == null) {
            HashMap<String, Promo> allPromos = new HashMap<>();
            for (Promo promo : promoService.getPromosMap().values()) {
                allPromos.put(promo.getName(), promo);
            }
            this.usersPromo.put(lineId, allPromos);
        }
    }

    public void setPromosByLineId(String lineId, HashMap<String, Promo> promos) {
        this.usersPromo.put(lineId, promos);
    }

    public void useUserPromo(String lineId, String promoName) {
        Promo promoUsed = usersPromo.get(lineId).get(promoName);
        promoUsed.addCount(-1);
        if (promoUsed.getCount() == 0) {
            usersPromo.get(lineId).remove(promoName);
        }
    }

    public Promo getUsedPromo(String lineId) {
        return usersUsedPromo.get(lineId);
    }

    public Promo usedPromo(String lineId, String name) {
        Promo promo = usersPromo.get(lineId).get(name);
        usersUsedPromo.put(lineId, promo);
        return promo;
    }

    public void cancelPromo(String lineId) {
        usersUsedPromo.remove(lineId);
    }


    public OrderState getStateByStateName(String stateName) {
        switch (stateName) {
            case "IDLE_ORDER_STATE":
                return this.idleOrderState;
            case "MAKE_ORDER_STATE":
                return this.makeOrderState;
            case "PROCESS_ORDER_STATE":
                return this.processOrderState;
            case "USE_PROMO_STATE":
                return this.usePromoState;
            default:
                return null;
        }
    }
}
