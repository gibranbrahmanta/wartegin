package com.herokuapp.wartegin.state;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.composite.MenuComponent;
import com.herokuapp.wartegin.messages.ConfirmTemplateFlexMessageSupplier;
import com.herokuapp.wartegin.messages.MenuFlexMessageSupplier;
import com.herokuapp.wartegin.messages.MenuNavigationFlexMessageSupplier;
import com.herokuapp.wartegin.messages.PromoListFlexMessageSupplier;
import com.herokuapp.wartegin.messages.state.IdleOrderStateMessages;
import com.linecorp.bot.model.message.Message;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//TODO : Make return message for each method
@Component
public class IdleOrderState extends OrderState {

    public static final String STATE_NAME = "IDLE_ORDER_STATE";

    @Autowired
    private IdleOrderStateMessages idleOrderStateMessages;

    @Override
    public List<Message> addItem(MenuComponent item, String lineId) {
        this.clearResponses();
        this.responses.add(this.idleOrderStateMessages.getAddItemMessage());
        return responses;
    }

    @Override
    public List<Message> deleteItem(String item, String lineId) {
        this.clearResponses();
        this.responses.add(this.idleOrderStateMessages.getDeleteItemMessage());
        return responses;
    }

    @Override
    public List<Message> makeOrder(String lineId) {
        this.orderStateHelper.setInitialPromosByLineId(lineId);

        if (this.orderStateHelper.driverVacancy()) {
            this.clearResponses();
            OrderState makeOrderState =
                    this.orderStateHelper.getStateByStateName(MakeOrderState.STATE_NAME);
            this.orderStateHelper.setUserStateByLineId(lineId, makeOrderState);
            this.orderStateHelper.setUserMenuByLineId(lineId, new Menu("Ordered Menu"));
            this.responses.add(this.idleOrderStateMessages.getMakeOrderMessage());

            this.responses.add(
                    new MenuFlexMessageSupplier(this.menuService.findAllFood(),
                            this.menuService.findAllDrink()).get()
            );
            this.responses.add(
                    new MenuNavigationFlexMessageSupplier().get()
            );
            this.responses.add(
                    new ConfirmTemplateFlexMessageSupplier(
                            "Apakah Anda ingin mengkonfirmasi pesanan Anda?",
                            "confirmOrder", "Pesanan saya konfirmasi.",
                            "continueOrder", "Pesanan saya lanjutkan.").get()
            );
        } else {
            this.clearResponses();
            this.responses.add(
                    this.idleOrderStateMessages.getTryAgainLaterMessage()
            );
        }
        return responses;
    }

    @Override
    public List<Message> confirmOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.idleOrderStateMessages.getConfirmOrderMessage());
        return responses;
    }

    @Override
    public List<Message> cancelOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.idleOrderStateMessages.getCancelOrderMessage());
        return responses;
    }

    @Override
    public List<Message> finishOrder(String lineId) {
        this.clearResponses();
        this.responses.add(this.idleOrderStateMessages.getFinishOrderMessage());
        return responses;
    }

    @Override
    public List<Message> showOrderList(String lineId) {
        this.clearResponses();
        this.responses.add(this.idleOrderStateMessages.getShowOrderMessage());
        return responses;
    }

    @Override
    public List<Message> usedPromo(String lineId)  {
        this.clearResponses();
        this.responses.add(this.idleOrderStateMessages.getUsedPromoMessage());
        return responses;
    }

    @Override
    public List<Message> usePromo(String lineId, String name) {
        this.clearResponses();
        this.responses.add(this.idleOrderStateMessages.getUsePromoMessage());
        return responses;
    }

    @Override
    public List<Message> seePromo(String lineId) {
        this.clearResponses();
        this.orderStateHelper.setInitialPromosByLineId(lineId);
        this.responses.add(
                new PromoListFlexMessageSupplier(this.orderStateHelper.getUserPromos(lineId)).get()
        );
        return responses;
    }

    @Override
    public List<Message> cancelPromo(String lineId) {
        this.clearResponses();
        this.responses.add(this.idleOrderStateMessages.getCancelPromoMessage());
        return responses;
    }
}
