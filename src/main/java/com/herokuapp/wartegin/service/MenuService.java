package com.herokuapp.wartegin.service;

import com.herokuapp.wartegin.model.Menu;
import java.util.List;
import java.util.Optional;

public interface MenuService {

    Optional<Menu> findMenu(long id);

    List<Menu> findAll();

    Menu register(Menu menu);

    Menu rewrite(Menu menu);

    void erase(long id);

    boolean isValid(Menu menu);

    List<Menu> findAllFood();

    List<Menu> findAllDrink();
}
