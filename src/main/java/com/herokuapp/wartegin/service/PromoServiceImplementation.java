package com.herokuapp.wartegin.service;

import com.herokuapp.wartegin.model.Promo;
import com.herokuapp.wartegin.repository.PromoRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PromoServiceImplementation implements PromoService {

    private final PromoRepository promoRepository;

    @Autowired
    public PromoServiceImplementation(PromoRepository promoRepository) {
        this.promoRepository = promoRepository;
        setPromos();
    }

    public void setPromos() {
        ArrayList<Promo> promoArrayList = new ArrayList<>();
        Promo ramadhanPromo =
                new Promo("Diskon Hepi 50%", "Dapatkan diskon 50% untuk semua item menu!",
                        3, 0, 0.5);
        Promo quarantinePromo =
                new Promo("Promo PSBB Meriah", "Dapatkan potongan Rp5.000!",
                        3, 5000, 1);
        Promo finalsPromo =
                new Promo("Diskon Akhir Semester", "Dapatkan diskon 10% untuk semua item menu!",
                        3, 0, 0.9);

        promoArrayList.add(ramadhanPromo);
        promoArrayList.add(quarantinePromo);
        promoArrayList.add(finalsPromo);

        this.promoRepository.addPromos(promoArrayList);
    }

    public List<Promo> getPromosList() {
        return new ArrayList<Promo>(this.promoRepository.getPromos().values());
    }

    public Map<String, Promo> getPromosMap() {
        return this.promoRepository.getPromos();
    }


    public String getPromosString() {
        String description = "";
        for (Promo promo : this.promoRepository.getPromos().values()) {
            description += promo.getName() + "\n";
            description += promo.getDescription() + "\n";
            description += "\n";
        }
        return description;
    }


}
