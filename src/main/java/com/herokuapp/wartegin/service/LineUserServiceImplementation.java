package com.herokuapp.wartegin.service;

import com.herokuapp.wartegin.model.LineUser;
import com.herokuapp.wartegin.repository.LineUserRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LineUserServiceImplementation implements LineUserService {

    @Autowired
    private LineUserRepository lineUserRepository;

    @Override
    public List<LineUser> findAll() {
        return lineUserRepository.findAll();
    }

    @Override
    public Optional<LineUser> findUserById(String id) {
        return lineUserRepository.findById(id);
    }

    @Override
    public void erase(String id) {
        lineUserRepository.deleteById(id);
    }

    @Override
    public LineUser rewrite(LineUser lineUser) {
        return lineUserRepository.save(lineUser);
    }

    @Override
    public LineUser register(LineUser lineUser) {
        return lineUserRepository.save(lineUser);
    }

    @Override
    public boolean isValid(LineUser lineUser) {
        return lineUser != null;
    }
}
