package com.herokuapp.wartegin.service;

import com.herokuapp.wartegin.model.Driver;
import com.herokuapp.wartegin.repository.DriverRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DriverServiceImplementation implements DriverService {

    private final DriverRepository driverRepository;

    @Autowired
    public DriverServiceImplementation(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
        setDrivers();
    }


    public List<Driver> getDrivers() {
        return this.driverRepository.getDriversList();
    }

    public void setDrivers() {
        ArrayList<Driver> driverArrayList = new ArrayList<>();
        Driver jokoPrama = new Driver("Joko Prama", "B6364KUS");
        Driver dodoPriyadi = new Driver("Dodo Priyadi", "B8991LXC");
        Driver akbarZaenal = new Driver("Akbar Zaenal", "B2675LUD");
        Driver edoWastara = new Driver("Edo Wastara", "B1733GSV");
        Driver fajarRawikara = new Driver("Fajar Rawikara", "B5776BZY");
        Driver syarifNugraha = new Driver("Syarif Nugraha", "B1802NVC");
        Driver budiPrasetyo = new Driver("Budi Prasetyo", "B4315OQQ");
        Driver timothyObaja = new Driver("Timothy Obaja", "B2890APH");
        Driver gilangRamadhan = new Driver("Gilang Ramadhan", "B6330GLI");
        Driver bagusRizal = new Driver("Bagus Rizal", "B0073MDD");

        driverArrayList.add(jokoPrama);
        driverArrayList.add(dodoPriyadi);
        driverArrayList.add(akbarZaenal);
        driverArrayList.add(edoWastara);
        driverArrayList.add(fajarRawikara);
        driverArrayList.add(syarifNugraha);
        driverArrayList.add(budiPrasetyo);
        driverArrayList.add(timothyObaja);
        driverArrayList.add(gilangRamadhan);
        driverArrayList.add(bagusRizal);

        this.driverRepository.addDrivers(driverArrayList);
        this.driverRepository.addVacantDrivers(driverArrayList);
    }

    public Driver occupyDriver(String name) {
        return driverRepository.getVacantDriversMap().remove(name);
    }

    public Driver vacantDriver(Driver driver) {
        return driverRepository.addVacantDriver(driver);
    }

    public ArrayList<Driver> getVacantDriversList() {
        return driverRepository.getVacantDriversList();
    }

    public Driver getRandomVacantDriver() {
        Map<String, Driver> vacantDrivers = driverRepository.getVacantDriversMap();
        ArrayList<String> temp = new ArrayList<>(vacantDrivers.keySet());
        Random random = new Random();
        int idx = random.nextInt(temp.size());
        return vacantDrivers.get(temp.get(idx));
    }
}
