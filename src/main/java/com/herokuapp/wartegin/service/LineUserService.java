package com.herokuapp.wartegin.service;

import com.herokuapp.wartegin.model.LineUser;
import java.util.List;
import java.util.Optional;

public interface LineUserService {
    List<LineUser> findAll();

    Optional<LineUser> findUserById(String id);

    void erase(String id);

    LineUser rewrite(LineUser lineUser);

    LineUser register(LineUser lineUser);

    boolean isValid(LineUser lineUser);
}
