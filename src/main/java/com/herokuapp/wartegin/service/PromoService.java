package com.herokuapp.wartegin.service;

import com.herokuapp.wartegin.model.Promo;
import java.util.List;
import java.util.Map;

public interface PromoService {
    void setPromos();

    Map<String, Promo> getPromosMap();

    String getPromosString();

    List<Promo> getPromosList();
}