package com.herokuapp.wartegin.service;

import com.herokuapp.wartegin.model.Driver;
import java.util.ArrayList;
import java.util.List;

public interface DriverService {
    void setDrivers();

    List<Driver> getDrivers();

    Driver occupyDriver(String name);

    Driver vacantDriver(Driver driver);

    Driver getRandomVacantDriver();

    ArrayList<Driver> getVacantDriversList();
}
