package com.herokuapp.wartegin.service;

import com.herokuapp.wartegin.model.Menu;
import com.herokuapp.wartegin.repository.MenuRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuServiceImplementation implements MenuService {

    @Autowired
    private MenuRepository menuRepository;

    @Override
    public Optional<Menu> findMenu(long id) {
        return menuRepository.findById(id);
    }

    @Override
    public List<Menu> findAll() {
        return menuRepository.findAll();
    }

    @Override
    public Menu register(Menu menu) {
        return menuRepository.save(menu);
    }

    @Override
    public Menu rewrite(Menu menu) {
        return menuRepository.save(menu);
    }

    @Override
    public void erase(long id) {
        menuRepository.deleteById(id);
    }

    @Override
    public List<Menu> findAllFood() {
        return menuRepository.findAllFood();
    }

    @Override
    public List<Menu> findAllDrink() {
        return menuRepository.findAllDrink();
    }

    @Override
    public boolean isValid(Menu menu) {
        if (menu == null) {
            return false;
        } else {
            return !menu.getName().equals("")
                    && menu.getPrice() >= 0
                    && (menu.getType() == 1 || menu.getType() == 2);
        }
    }

}
