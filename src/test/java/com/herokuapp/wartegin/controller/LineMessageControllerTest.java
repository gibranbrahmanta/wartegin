package com.herokuapp.wartegin.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;

import com.herokuapp.wartegin.composite.MenuComponent;
import com.herokuapp.wartegin.model.LineUser;
import com.herokuapp.wartegin.service.LineUserService;
import com.herokuapp.wartegin.service.MenuService;
import com.herokuapp.wartegin.state.OrderState;
import com.herokuapp.wartegin.state.helper.OrderStateHelper;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import com.linecorp.bot.model.response.BotApiResponse;
import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;

@ExtendWith(MockitoExtension.class)
public class LineMessageControllerTest {

    @Mock
    OrderStateHelper orderStateHelper;
    @Mock
    OrderState orderState;
    @Mock
    CompletableFuture<UserProfileResponse> userProfileCF;
    @Mock
    CompletableFuture<BotApiResponse> botApiCF;
    @Mock
    LineUserService lineUserService;
    @Mock
    MenuService menuService;
    @Mock
    LineMessagingClient lineMessagingClient;
    @InjectMocks
    LineMessageController lineMessageController;
    @Value("${line.bot.channel-token}")
    String channelAccessToken;
    @Captor
    ArgumentCaptor<ReplyMessage> replyMessageArgumentCaptor;
    @Captor
    ArgumentCaptor<MenuComponent> menuComponentArgumentCaptor;
    @Captor
    ArgumentCaptor<String> stringCaptor;
    UserProfileResponse userProfileResponse;
    LineUser lineUser;
    ArrayList<String> details;
    ArrayList<Message> responseMessages;

    private MessageEvent<TextMessageContent> createDummyMessageEvent(String text, String userId) {
        return MessageEvent.<TextMessageContent>builder()
                .replyToken("replyToken")
                .timestamp(Instant.now())
                .source(
                        UserSource.builder()
                                .userId(userId)
                                .build()
                )
                .message(
                        TextMessageContent.builder()
                                .id("id")
                                .text(text)
                                .build()
                )
                .build();
    }

    private PostbackEvent createDummyPostbackEvent(String data, String userId) {
        return PostbackEvent.builder()
                .timestamp(Instant.now())
                .source(
                        UserSource.builder()
                        .userId(userId)
                        .build()
                ).replyToken("replyToken")
                .postbackContent(
                        PostbackContent.builder()
                                .data(data)
                                .params(new HashMap<>())
                                .build()
                )
                .build();
    }

    private UserProfileResponse createDummyUserProfileResponse() {
        return new UserProfileResponse(
                "user 1", "1", URI.create("image.com"), "Status"
        );
    }

    private void prepareGetUserDisplayName() throws ExecutionException, InterruptedException {
        given(lineMessagingClient.getProfile(userProfileResponse.getUserId()))
                .willReturn(userProfileCF);
        given(lineMessagingClient.getProfile(userProfileResponse.getUserId()).get())
                .willReturn(userProfileResponse);
    }

    private void prepareTextMessageEvent() throws ExecutionException, InterruptedException {
        prepareGetUserDisplayName();
        given(lineUserService.findUserById(userProfileResponse.getUserId()))
                .willReturn(Optional.ofNullable(null))
                .willReturn(Optional.of(lineUser));
        given(lineMessagingClient.replyMessage(any(ReplyMessage.class))).willReturn(botApiCF);
        given(botApiCF.get()).willReturn(
                new BotApiResponse("requestId", "message", details)
        );

    }

    private void preparePostbackMessageEvent() throws ExecutionException, InterruptedException {
        given(orderStateHelper.getUserStateByLineId(userProfileResponse.getUserId()))
                .willReturn(orderState);
        given(lineMessagingClient.replyMessage(any(ReplyMessage.class))).willReturn(botApiCF);
        given(botApiCF.get()).willReturn(
                new BotApiResponse("requestId", "message", details)
        );
    }

    @BeforeEach
    public void setUp() throws ExecutionException, InterruptedException {
        userProfileResponse = this.createDummyUserProfileResponse();
        lineUser = new LineUser("user 1", "1", 1);
        details = new ArrayList<>();
        details.add("detail 1");
        details.add("detail 2");
        responseMessages = new ArrayList<>();
        responseMessages.add(TextMessage.builder()
                .text("Message 1")
                .build());
        responseMessages.add(TextMessage.builder()
                .text("Message 2")
                .build());
    }

    @Test
    public void testGetUserDisplayName() throws ExecutionException, InterruptedException {
        prepareGetUserDisplayName();
        assertEquals(lineMessageController.getUserDisplayName("1"), "user 1");
    }

    @Test
    public void testExceptionBotResp() {
        assertThrows(RuntimeException.class, this::testFailedBotResp);
    }

    public void testFailedBotResp() throws ExecutionException, InterruptedException {
        prepareGetUserDisplayName();
        given(lineUserService.findUserById(userProfileResponse.getUserId()))
                .willReturn(Optional.of(lineUser));
        given(lineMessagingClient.replyMessage(any(ReplyMessage.class))).willReturn(botApiCF);
        given(botApiCF.get()).willThrow(InterruptedException.class);

        MessageEvent<TextMessageContent> messageEvent = this.createDummyMessageEvent("/nav", "1");
        lineMessageController.handleTextEvent(messageEvent);

        then(lineMessagingClient).should().replyMessage(replyMessageArgumentCaptor.capture());
    }

    @Test
    public void testTextCommandMbake() throws ExecutionException, InterruptedException {
        prepareTextMessageEvent();
        MessageEvent<TextMessageContent> messageEvent = this.createDummyMessageEvent("mbake!", "1");
        lineMessageController.handleTextEvent(messageEvent);
        then(lineMessagingClient).should().replyMessage(replyMessageArgumentCaptor.capture());
    }

    @Test
    public void testTextCommandRandom() throws ExecutionException, InterruptedException {
        prepareTextMessageEvent();
        MessageEvent<TextMessageContent> messageEvent = this.createDummyMessageEvent("blabla", "1");
        lineMessageController.handleTextEvent(messageEvent);
        then(lineMessagingClient).should().replyMessage(replyMessageArgumentCaptor.capture());
    }

    @Test
    public void testPostbackCommandAddItem() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        given(orderState.addItem(menuComponentArgumentCaptor.capture(),
                       stringCaptor.capture())
        ).willReturn(responseMessages);

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"addItem\",\"name\":\"Roti\",\"price\":\"100\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandDeleteItem() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        given(orderState.deleteItem(stringCaptor.capture(),
                stringCaptor.capture())
        ).willReturn(responseMessages);

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"deleteItem\",\"name\":\"Roti\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandFinishOrder() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        given(orderState.finishOrder(stringCaptor.capture())
        ).willReturn(responseMessages);

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"finishOrder\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandConfirmOrder() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        given(orderState.confirmOrder(stringCaptor.capture())
        ).willReturn(responseMessages);

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"confirmOrder\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandCancelOrder() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        given(orderState.cancelOrder(stringCaptor.capture()))
                .willReturn(responseMessages);

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"cancelOrder\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandMakeOrder() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        given(orderState.makeOrder(stringCaptor.capture())).willReturn(responseMessages);

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"makeOrder\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandContinueOrder() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"continueOrder\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandNotArrived() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"notArrived\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandShowOrderList() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        given(orderState.showOrderList(stringCaptor.capture())).willReturn(responseMessages);

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"showOrderList\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandSeePromo() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        given(orderState.seePromo(stringCaptor.capture())).willReturn(responseMessages);

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"seePromo\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandUsePromo() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        given(orderState.usePromo(stringCaptor.capture(), stringCaptor.capture()))
                .willReturn(responseMessages);

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"usePromo\",\"name\":\"promo1\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandUsedPromo() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"usedPromo\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandCancelPromo() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();
        given(orderState.cancelPromo(stringCaptor.capture())).willReturn(responseMessages);

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"cancelPromo\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandAboutWartegin() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"aboutWartegin\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

    @Test
    public void testPostbackCommandRandom() throws ExecutionException, InterruptedException {
        preparePostbackMessageEvent();

        PostbackEvent postbackEvent = this.createDummyPostbackEvent(
                "{\"command\":\"randomAja\"}",
                userProfileResponse.getUserId());

        lineMessageController.handlePostbackEvent(postbackEvent);
    }

}
