package com.herokuapp.wartegin.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.herokuapp.wartegin.model.Menu;
import com.herokuapp.wartegin.service.MenuService;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;

@WebMvcTest(controllers = MenuController.class)
class MenuControllerTest {

    @Autowired
    public MockMvc mockMvc;
    @Captor
    ArgumentCaptor<Menu> menuCaptor;
    @MockBean
    private MenuService menuService;

    @Test
    public void testFindAll() throws Exception {
        mockMvc.perform(get("/menu"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(new ResultHandler() {
                    @Override
                    public void handle(MvcResult result) throws Exception {
                        assertNotNull(result.getResponse().getContentAsString());
                    }
                });
    }

    @Test
    public void testCreateSuccess() throws Exception {
        Menu menu = new Menu("test", 1, 1);
        menu.setId(1);

        when(menuService.isValid(menuCaptor.capture())).thenReturn(true);
        when(menuService.register(menuCaptor.capture())).thenReturn(menu);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(menu);

        mockMvc.perform(post("/menu")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateFailed() throws Exception {
        Menu menu = new Menu("test", 1, 1);
        menu.setId(1);

        when(menuService.isValid(menuCaptor.capture())).thenReturn(false);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(menu);

        mockMvc.perform(post("/menu")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testUpdateSuccess() throws Exception {
        Menu menu = new Menu("Test", 1, 1);
        menu.setId(1);

        when(menuService.isValid(menuCaptor.capture())).thenReturn(true);
        when(menuService.rewrite(menuCaptor.capture())).thenReturn(menu);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(menu);

        mockMvc.perform(put("/menu/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateFailed() throws Exception {
        Menu menu = new Menu("Test", 1, 1);
        menu.setId(1);

        when(menuService.isValid(menuCaptor.capture())).thenReturn(false);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(menu);

        mockMvc.perform(put("/menu/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateFailed2() throws Exception {
        Menu menu = new Menu("Test", 1, 1);
        menu.setId(1);

        when(menuService.isValid(menuCaptor.capture())).thenReturn(true);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(menu);

        mockMvc.perform(put("/menu/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateFailed3() throws Exception {
        Menu menu = new Menu("Test", 1, 1);
        menu.setId(1);

        when(menuService.isValid(menuCaptor.capture())).thenReturn(false);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(menu);

        mockMvc.perform(put("/menu/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testFindByIdSuccess() throws Exception {
        Menu menu = new Menu("Test", 1, 1);
        menu.setId(1);

        when(menuService.findMenu(menu.getId())).thenReturn(Optional.of(menu));

        mockMvc.perform(get("/menu/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void testFindByIdFailed() throws Exception {
        Menu menu = new Menu("Test", 1, 1);
        menu.setId(1);

        when(menuService.findMenu(menu.getId())).thenReturn(Optional.empty());

        mockMvc.perform(get("/menu/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelete() throws Exception {
        mockMvc.perform(delete("/menu/1"))
                .andExpect(status().isOk());

        then(menuService).should().erase(1);
    }
}
