package com.herokuapp.wartegin.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.herokuapp.wartegin.model.LineUser;
import com.herokuapp.wartegin.service.LineUserService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;


@WebMvcTest(controllers = LineUserController.class)
class LineUserControllerTest {

    @Autowired
    public MockMvc mockMvc;

    @MockBean
    LineUserService lineUserService;

    @Captor
    ArgumentCaptor<LineUser> userCaptor;

    @Test
    public void testFindAll() throws Exception {
        List<LineUser> lineUsers = new ArrayList<>();
        LineUser user = new LineUser("test", "a1", 1);
        lineUsers.add(user);

        when(lineUserService.findAll()).thenReturn(lineUsers);
        mockMvc.perform(get("/user"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(new ResultHandler() {
                    @Override
                    public void handle(MvcResult result) throws Exception {
                        assertNotNull(result.getResponse().getContentAsString());
                    }
                });
    }

    @Test
    public void testCreateSuccess() throws Exception {
        LineUser user = new LineUser("Test", "a1", 1);

        doReturn(true).when(lineUserService).isValid(userCaptor.capture());
        when(lineUserService.register(userCaptor.capture())).thenReturn(user);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/user").contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateFailed() throws Exception {
        LineUser user = new LineUser("Test", "a1", 1);

        when(lineUserService.isValid(userCaptor.capture())).thenReturn(false);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/user").contentType(MediaType.APPLICATION_JSON).content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testFindByIdSuccess() throws Exception {
        LineUser user = new LineUser("Test", "a1", 1);
        when(lineUserService.findUserById(user.getLineId())).thenReturn(Optional.of(user));

        mockMvc.perform(get("/user/a1"))
                .andExpect(status().isOk());
    }

    @Test
    public void testFindByIdFailed() throws Exception {
        LineUser user = new LineUser("Test", "a1", 1);
        when(lineUserService.findUserById(user.getLineId())).thenReturn(Optional.empty());

        mockMvc.perform(get("/user/a1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateSuccess() throws Exception {
        LineUser user = new LineUser("Test", "a1", 1);
        doReturn(true).when(lineUserService).isValid(userCaptor.capture());
        when(lineUserService.rewrite(userCaptor.capture())).thenReturn(user);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(user);

        mockMvc.perform(put("/user/a1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateFailed() throws Exception {
        LineUser user = new LineUser("Test", "1", 1);
        when(lineUserService.isValid(userCaptor.capture())).thenReturn(false);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(user);

        mockMvc.perform(put("/user/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateFailed2() throws Exception {
        LineUser user = new LineUser("Test", "12", 1);
        when(lineUserService.isValid(userCaptor.capture())).thenReturn(false);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(user);

        mockMvc.perform(put("/user/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateFailed3() throws Exception {
        LineUser user = new LineUser("Test", "2", 1);
        when(lineUserService.isValid(userCaptor.capture())).thenReturn(true);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(user);

        mockMvc.perform(put("/user/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDelete() throws Exception {
        mockMvc.perform(delete("/user/1"))
                .andExpect(status().isOk());
    }
}
