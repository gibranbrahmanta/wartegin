package com.herokuapp.wartegin.composite;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class MenuTest {

    private Menu menu;

    @BeforeEach
    public void setUp() {
        menu = new Menu("tester");
        MenuComponent a = new MenuItem("a", 2000);
        menu.add(a);
        MenuComponent b = new MenuItem("b", 3000);
        menu.add(b);
        MenuComponent c = new MenuItem("c", 4000);
        menu.add(c);
    }

    @Test
    void getName() {
        assertEquals("tester", menu.getName());
    }

    @Test
    void getTotalMenuComponents() {
        assertEquals(3, menu.getTotalMenuComponents());
    }

    @Test
    void getPrice() {
        assertEquals(9000, menu.getPrice());
    }

    @Test
    void getDescription() {
        assertEquals("a\nb\nc\n", menu.getDescription());
    }

    @Test
    void remove() {
        menu.remove("c");
        assertEquals(2, menu.getTotalMenuComponents());

        menu = new Menu("Menu 2");
        menu.remove("a");
        assertEquals(0, menu.getTotalMenuComponents());
    }

    @Test
    void add() {
        MenuComponent c = new MenuItem("c", 4000);
        menu.add(c);
        assertEquals(4, menu.getTotalMenuComponents());
    }
}