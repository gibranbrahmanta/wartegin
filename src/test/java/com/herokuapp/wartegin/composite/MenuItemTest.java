package com.herokuapp.wartegin.composite;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MenuItemTest {

    private MenuItem menu;

    @BeforeEach
    public void setUp() {
        menu = new MenuItem("abc", 5000);
    }

    @Test
    void getPrice() {
        assertEquals(5000, menu.getPrice());
    }

    @Test
    void getName() {
        assertEquals("abc", menu.getName());
    }

}




