package com.herokuapp.wartegin.state;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import com.herokuapp.wartegin.composite.MenuItem;
import com.herokuapp.wartegin.messages.state.IdleOrderStateMessages;
import com.herokuapp.wartegin.model.Menu;
import com.herokuapp.wartegin.model.Promo;
import com.herokuapp.wartegin.service.MenuService;
import com.herokuapp.wartegin.state.helper.OrderStateHelper;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class IdleOrderStateTest {

    @Mock
    MakeOrderState makeOrderState;

    @Mock
    MenuService menuService;

    @Mock
    OrderStateHelper orderStateHelper;

    @Spy
    ArrayList<Message> responses;

    @Mock
    IdleOrderStateMessages idleOrderStateMessages;

    @InjectMocks
    IdleOrderState idleOrderState;

    @Captor
    ArgumentCaptor<Message> messageArgumentCaptor;

    MenuItem menuItem;
    String lineId;
    String item;
    List<Menu> foodList;
    List<Menu> drinkList;
    Promo promo;

    @BeforeEach
    public void setUp() {
        menuItem = new MenuItem("aa", 1);
        lineId = "id1";
        item = "item1";

        foodList = new ArrayList<>();
        drinkList = new ArrayList<>();

        Menu food1 = new Menu("food1", 1, 1);
        Menu food2 = new Menu("food2", 2, 1);

        foodList.add(food1);
        foodList.add(food2);

        Menu drink1 = new Menu("drink1", 1, 2);
        Menu drink2 = new Menu("drink2", 2, 2);

        drinkList.add(drink1);
        drinkList.add(drink2);

        promo = new Promo("promo", "test", 1, 1000, 1);

    }

    @Test
    public void testAddItem() {
        given(idleOrderStateMessages.getAddItemMessage()).willReturn(
                new TextMessage("You must 'Make Order' first to add items to your Order List"));
        List<Message> addMessages = idleOrderState.addItem(menuItem, lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(idleOrderStateMessages.getAddItemMessage(), messageArgumentCaptor.getValue());
        assertEquals(1, addMessages.size());
    }

    @Test
    public void testDeleteItem() {
        given(idleOrderStateMessages.getDeleteItemMessage()).willReturn(
                new TextMessage(
                        "You must 'Make Order' first to delete items from your Order List"));
        List<Message> deleteMessages = idleOrderState.deleteItem(item, lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(idleOrderStateMessages.getDeleteItemMessage(),
                messageArgumentCaptor.getValue());
        assertEquals(1, deleteMessages.size());
    }

    @Test
    public void testMakeOrderDriverFull() {
        given(orderStateHelper.driverVacancy()).willReturn(false);
        List<Message> makeOrderMessages = idleOrderState.makeOrder(lineId);

        then(responses).should(times(1)).add(messageArgumentCaptor.capture());
        assertNotNull(messageArgumentCaptor.getAllValues());
        assertEquals(1, makeOrderMessages.size());
    }

    @Test
    public void testMakeOrderDrierAvailable() {
        given(orderStateHelper.driverVacancy()).willReturn(true);
        given(orderStateHelper.getStateByStateName(MakeOrderState.STATE_NAME))
                .willReturn(makeOrderState);
        given(idleOrderStateMessages.getMakeOrderMessage())
                .willReturn(new TextMessage(
                        "Here are our Menu! You can order your favourite menu here!"));
        given(menuService.findAllDrink()).willReturn(drinkList);
        given(menuService.findAllFood()).willReturn(foodList);

        List<Message> makeOrderMessages = idleOrderState.makeOrder(lineId);

        then(responses).should(times(4)).add(messageArgumentCaptor.capture());
        assertNotNull(messageArgumentCaptor.getAllValues());
        assertEquals(4, makeOrderMessages.size());
    }

    @Test
    public void testConfirmOrder() {
        given(idleOrderStateMessages.getConfirmOrderMessage())
                .willReturn(new TextMessage("You must 'Make Order' first to Confirm your Order"));
        List<Message> confirmOrderMessages = idleOrderState.confirmOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(idleOrderStateMessages.getConfirmOrderMessage(),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) confirmOrderMessages.get(0);
        assertEquals(idleOrderStateMessages.getConfirmOrderMessage().getText(), message.getText());

        assertEquals(1, confirmOrderMessages.size());
    }

    @Test
    public void testCancelOrder() {
        given(idleOrderStateMessages.getCancelOrderMessage())
                .willReturn(new TextMessage("You must 'Make Order' first to Cancel your Order"));
        List<Message> cancelOrderMessages = idleOrderState.cancelOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(idleOrderStateMessages.getCancelOrderMessage(),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) cancelOrderMessages.get(0);
        assertEquals(idleOrderStateMessages.getCancelOrderMessage().getText(), message.getText());

        assertEquals(1, cancelOrderMessages.size());
    }

    @Test
    public void testFinishOrder() {
        given(idleOrderStateMessages.getFinishOrderMessage()).willReturn(new TextMessage(
                "You must 'Make Order & Confirm Order' then wait for your Order to be finished"));
        List<Message> finishOrderMessage = idleOrderState.finishOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(idleOrderStateMessages.getFinishOrderMessage(),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) finishOrderMessage.get(0);
        assertEquals(idleOrderStateMessages.getFinishOrderMessage().getText(), message.getText());

        assertEquals(1, finishOrderMessage.size());
    }

    @Test
    public void testShowOrderList() {
        given(idleOrderStateMessages.getShowOrderMessage())
                .willReturn(new TextMessage("You don't have any order now!"));
        List<Message> showOrderListMessage = idleOrderState.showOrderList(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(idleOrderStateMessages.getShowOrderMessage(),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) showOrderListMessage.get(0);
        assertEquals(idleOrderStateMessages.getShowOrderMessage().getText(), message.getText());

        assertEquals(1, showOrderListMessage.size());
    }

    @Test
    public void testUsedPromo() {
        given(idleOrderStateMessages.getUsedPromoMessage())
                .willReturn(new TextMessage("You must 'Make Order' first to use promos"));
        List<Message> usedPromoMessage = idleOrderState.usedPromo(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(idleOrderStateMessages.getUsedPromoMessage(),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) usedPromoMessage.get(0);
        assertEquals(idleOrderStateMessages.getUsedPromoMessage().getText(), message.getText());

        assertEquals(1, usedPromoMessage.size());
    }

    @Test
    public void testUsePromo() {
        given(idleOrderStateMessages.getUsePromoMessage())
                .willReturn(new TextMessage("You must 'Make Order' first to use promos"));
        List<Message> usePromoMessage = idleOrderState.usePromo(lineId, promo.getName());

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(idleOrderStateMessages.getUsePromoMessage(),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) usePromoMessage.get(0);
        assertEquals(idleOrderStateMessages.getUsePromoMessage().getText(), message.getText());

        assertEquals(1, usePromoMessage.size());
    }

    @Test
    public void testCancelPromo() {
        given(idleOrderStateMessages.getCancelPromoMessage())
                .willReturn(new TextMessage("You must 'Make Order' first to use promos"));
        List<Message> cancelPromoMessage = idleOrderState.cancelPromo(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(idleOrderStateMessages.getCancelPromoMessage(),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) cancelPromoMessage.get(0);
        assertEquals(idleOrderStateMessages.getCancelPromoMessage().getText(), message.getText());

        assertEquals(1, cancelPromoMessage.size());
    }

    @Test
    public void testSeePromo() {
        ArrayList<Promo> listOfPromos = new ArrayList<>();
        listOfPromos.add(promo);
        listOfPromos.add(promo);
        given(orderStateHelper.getUserPromos(lineId)).willReturn(listOfPromos);

        List<Message> seePromoMessages = idleOrderState.seePromo(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertNotNull(messageArgumentCaptor.getValue());
        assertEquals(seePromoMessages.size(), 1);
    }

}
