package com.herokuapp.wartegin.state;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.composite.MenuItem;
import com.herokuapp.wartegin.messages.state.ProcessOrderStateMessages;
import com.herokuapp.wartegin.model.Promo;
import com.herokuapp.wartegin.service.MenuService;
import com.herokuapp.wartegin.state.helper.OrderStateHelper;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class ProcessOrderStateTest {

    @Mock
    MenuService menuService;

    @Mock
    IdleOrderState idleOrderState;

    @Spy
    ArrayList<Message> responses;

    @Mock
    OrderStateHelper orderStateHelper;

    @Mock
    ProcessOrderStateMessages processOrderStateMessages;

    @InjectMocks
    ProcessOrderState processOrderState;

    @Captor
    ArgumentCaptor<Message> messageArgumentCaptor;

    Menu orderedMenu;
    MenuItem menuItem;
    String lineId;
    Promo promo;

    @BeforeEach
    public void setUp() {
        orderedMenu = new Menu("menu1");
        menuItem = new MenuItem("item1", 1);
        promo = new Promo("PromoNameTest", "PromoDescriptionTest", 1, 1000, 1);
        lineId = "id1";
    }

    @Test
    public void testAddItem() {
        given(processOrderStateMessages.getAddItemMessage()).willReturn(new TextMessage(
                "You cannot add item to your Order List "
                        + "because you are still having unfinished Order"));

        List<Message> addItemMessages = processOrderState.addItem(menuItem, lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, addItemMessages.size());
    }

    @Test
    public void testDeleteItem() {
        given(processOrderStateMessages.getDeleteItemMessage())
                .willReturn(new TextMessage("You cannot delete"
                        + " item from your Order List because "
                        + "you are still having unfinished Order"));

        List<Message> deleteItemMessages = processOrderState.deleteItem(menuItem.getName(), lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, deleteItemMessages.size());
    }

    @Test
    public void testMakeOrder() {
        given(processOrderStateMessages.getMakeOrderMessage())
                .willReturn(new TextMessage("You  are still having unfinished Order"));

        List<Message> makeOrderMessages = processOrderState.makeOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, makeOrderMessages.size());
    }

    @Test
    public void testConfirmOrder() {
        given(processOrderStateMessages.getConfirmOrderMessage())
                .willReturn(new TextMessage("Your Order is being processed by the system"));

        List<Message> confirmOrderMessages = processOrderState.confirmOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, confirmOrderMessages.size());
    }

    @Test
    public void testCancelOrder() {
        given(processOrderStateMessages.getCancelOrderMessage())
                .willReturn(new TextMessage(
                "You cannot cancel your Order because "
                        + "your Order is being processed by the system"));

        List<Message> cancelOrderMessages = processOrderState.cancelOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, cancelOrderMessages.size());
    }

    @Test
    public void testFinishOrder() {
        given(processOrderStateMessages.getFinishOrderMessage())
                .willReturn(new TextMessage(
                        "Your Order has been finished successfully. Thank you!"));
        given(orderStateHelper.getStateByStateName(IdleOrderState.STATE_NAME))
                .willReturn(idleOrderState);

        List<Message> finishOrderMessages = processOrderState.finishOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, finishOrderMessages.size());

    }

    @Test
    public void testShowOrderList() {
        given(orderStateHelper.getUserMenuByLineId(lineId)).willReturn(orderedMenu);
        orderedMenu.add(menuItem);
        orderedMenu.add(menuItem);

        List<Message> showListOrderMessages = processOrderState.showOrderList(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, responses.size());
    }

    @Test
    public void testUsedPromo() {
        given(orderStateHelper.getStringAllPromo()).willReturn("All Promo");
        List<Message> usedPromoMessage = processOrderState.usedPromo(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(usedPromoMessage.get(0),
                messageArgumentCaptor.getValue());

        assertTrue(usedPromoMessage.get(0) instanceof TextMessage);

        assertEquals(1, usedPromoMessage.size());
    }

    @Test
    public void testUsePromo() {
        given(processOrderStateMessages.getUsedPromoMessage())
                .willReturn(new TextMessage(
                        "You can't use promo. You have already checked out your Order"));
        List<Message> usePromoMessage = processOrderState.usePromo(lineId, promo.getName());
        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(usePromoMessage.get(0),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) usePromoMessage.get(0);
        assertEquals("You can't use promo. You have already checked out your Order",
                message.getText());

        assertEquals(1, usePromoMessage.size());
    }

    @Test
    public void testCancelPromo() {
        given(processOrderStateMessages.getCancelPromoMessage())
                .willReturn(new TextMessage(
                        "You can't use promo. You have already checked out your Order"));
        List<Message> cancelPromoMessage = processOrderState.cancelPromo(lineId);
        
        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(processOrderStateMessages.getCancelPromoMessage(),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) cancelPromoMessage.get(0);
        assertEquals("You can't use promo. You have already checked out your Order",
                message.getText());

        assertEquals(1, cancelPromoMessage.size());
    }

    @Test
    public void testSeePromo() {
        ArrayList<Promo> listOfPromos = new ArrayList<>();
        listOfPromos.add(promo);
        listOfPromos.add(promo);
        given(orderStateHelper.getUserPromos(lineId)).willReturn(listOfPromos);

        List<Message> seePromoMessages = processOrderState.seePromo(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertNotNull(messageArgumentCaptor.getValue());
        assertEquals(seePromoMessages.size(), 1);
    }
}
