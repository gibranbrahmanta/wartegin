package com.herokuapp.wartegin.state;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.composite.MenuItem;
import com.herokuapp.wartegin.messages.state.PromoOrderStateMessages;
import com.herokuapp.wartegin.model.Driver;
import com.herokuapp.wartegin.model.Promo;
import com.herokuapp.wartegin.service.MenuService;
import com.herokuapp.wartegin.state.helper.OrderStateHelper;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class UsePromoStateTest {

    @Mock
    MenuService menuService;

    @Mock
    IdleOrderState idleOrderState;

    @Spy
    ArrayList<Message> responses;

    @Mock
    OrderStateHelper orderStateHelper;

    @Mock
    ProcessOrderState processOrderState;

    @Mock
    PromoOrderStateMessages promoOrderStateMessages;

    @InjectMocks
    UsePromoState usePromoState;

    @Captor
    ArgumentCaptor<Message> messageArgumentCaptor;

    Menu orderedMenu;
    MenuItem menuItem;
    Promo promo;
    Promo promo2;
    Driver driver;
    String lineId;

    @BeforeEach
    public void setUp() {
        orderedMenu = new Menu("menu1");
        menuItem = new MenuItem("item1",10000);
        orderedMenu.add(menuItem);
        promo = new Promo("PromoNameTest", "PromoDescriptionTest", 1, 1000, 1);
        promo2 = new Promo("Promo2Name", "Ini Promo2", 1, 100000, 50);
        driver = new Driver("DriverNameTest", "PlateNumberTest");
        lineId = "id1";


    }

    @Test
    public void testAddItem() {
        given(promoOrderStateMessages.getAddItemMessage())
                .willReturn(new TextMessage(
                        "You cannot add item to your Order List because you have already ordered"));

        List<Message> addItemMessages = usePromoState.addItem(menuItem, lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, addItemMessages.size());
    }

    @Test
    public void testDeleteItem() {
        given(promoOrderStateMessages.getDeleteItemMessage())
                .willReturn(new TextMessage(
                        "You cannot delete item from your Order"
                                + " List because you have already ordered"
                ));

        List<Message> deleteItemMessages = usePromoState.deleteItem(menuItem.getName(), lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, deleteItemMessages.size());
    }

    @Test
    public void testMakeOrder() {
        given(promoOrderStateMessages.getMakeOrderMessage())
                .willReturn(new TextMessage(
                        "You cannot make your Order List because you have already ordered"));

        List<Message> makeOrderMessages = usePromoState.makeOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, makeOrderMessages.size());
    }

    @Test
    public void testConfirmOrder() {
        given(promoOrderStateMessages.getConfirmOrderMessage())
                .willReturn(new TextMessage(
                        "You cannot confirm your Order List because you have already ordered"));

        List<Message> confirmOrderMessages = usePromoState.confirmOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, confirmOrderMessages.size());
    }

    @Test
    public void testCancelOrder() {
        given(promoOrderStateMessages.getCancelOrderMessage())
                .willReturn(new TextMessage(
                        "You cannot cancel your Order List because you have already ordered"));

        List<Message> cancelOrderMessages = usePromoState.cancelOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, cancelOrderMessages.size());
    }

    @Test
    public void testFinishOrder() {
        given(promoOrderStateMessages.getFinishOrderMessage())
                .willReturn(new TextMessage(
                        "You cannot finish your Order List because you have already ordered"));

        List<Message> finishOrderMessages = usePromoState.finishOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, finishOrderMessages.size());

    }

    @Test
    public void testShowOrderList() {
        given(orderStateHelper.getUserMenuByLineId(lineId)).willReturn(orderedMenu);
        List<Message> finishOrderMessages = usePromoState.showOrderList(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, finishOrderMessages.size());


    }

    @Test
    public void testUsedPromo() {
        given(orderStateHelper.getUsedPromo(lineId)).willReturn(promo);
        given(orderStateHelper.getUserMenuByLineId(lineId)).willReturn(orderedMenu);
        given(orderStateHelper.getUsedPromo(lineId)).willReturn(promo).willReturn(promo2);
        given(orderStateHelper.getUserDriver()).willReturn(driver);
        given(promoOrderStateMessages.getUsedPromoMessage())
                .willReturn(new TextMessage("You have checked out a promo for this Order!"));

        List<Message> showUsedPromoMessages = usePromoState.usedPromo(lineId);

        assertEquals(3, showUsedPromoMessages.size());

        List<Message> showUsedPromoMessages2 = usePromoState.usedPromo(lineId);

        assertEquals(3, showUsedPromoMessages2.size());
    }

    @Test
    public void testUsePromo() {
        given(orderStateHelper.usedPromo(lineId, promo.getName())).willReturn(promo);

        List<Message> showUsePromoMessages = usePromoState.usePromo(lineId, promo.getName());

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, showUsePromoMessages.size());

    }

    @Test
    public void testCancelPromo() {
        given(orderStateHelper.getUsedPromo(lineId)).willReturn(promo);
        given(orderStateHelper.getUserMenuByLineId(lineId)).willReturn(orderedMenu);
        given(orderStateHelper.getUsedPromo(lineId)).willReturn(promo);
        given(orderStateHelper.getUserDriver()).willReturn(driver);
        given(promoOrderStateMessages.getCancelPromoMessage())
                .willReturn(new TextMessage("You have canceled to use a promo for this Order!"));

        List<Message> cancelPromoMessages = usePromoState.cancelPromo(lineId);

        then(responses).should(times(3)).add(messageArgumentCaptor.capture());
        assertEquals(3, cancelPromoMessages.size());
    }

    @Test
    public void testSeePromo() {
        ArrayList<Promo> listOfPromos = new ArrayList<>();
        listOfPromos.add(promo);
        listOfPromos.add(promo);
        given(orderStateHelper.getUserPromos(lineId)).willReturn(listOfPromos);

        List<Message> seePromoMessages = usePromoState.seePromo(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertNotNull(messageArgumentCaptor.getValue());
        assertEquals(seePromoMessages.size(), 1);
    }

}
