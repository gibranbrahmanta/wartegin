package com.herokuapp.wartegin.state.helper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.model.Driver;
import com.herokuapp.wartegin.model.Promo;
import com.herokuapp.wartegin.repository.DriverRepository;
import com.herokuapp.wartegin.repository.PromoRepository;
import com.herokuapp.wartegin.service.DriverService;
import com.herokuapp.wartegin.service.PromoService;
import com.herokuapp.wartegin.state.IdleOrderState;
import com.herokuapp.wartegin.state.MakeOrderState;
import com.herokuapp.wartegin.state.OrderState;
import com.herokuapp.wartegin.state.ProcessOrderState;
import com.herokuapp.wartegin.state.UsePromoState;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class OrderStateHelperTest {

    @Mock
    IdleOrderState idleOrderState;

    @Mock
    MakeOrderState makeOrderState;

    @Mock
    ProcessOrderState processOrderState;

    @Mock
    UsePromoState usePromoState;

    @Mock
    PromoRepository promoRepository;

    @Mock
    PromoService promoService;

    @Mock
    DriverRepository driverRepository;

    @Mock
    DriverService driverService;

    @Spy
    HashMap<String, OrderState> usersState;

    @Spy
    HashMap<String, Menu> usersOrderedMenu;

    @Spy
    HashMap<String, HashMap<String, Promo>> usersPromo;

    @Spy
    HashMap<String, Promo> usersUsedPromo;

    @Spy
    HashMap<String, Driver> usersDriver;

    @InjectMocks
    OrderStateHelper orderStateHelper;

    @Captor
    ArgumentCaptor<String> usersStateArgumentCaptor;

    @Captor
    ArgumentCaptor<String> stringArgumentCaptor;

    @Captor
    ArgumentCaptor<HashMap<String, Promo>> mapStringPromoCaptor;

    String lineId;
    Menu orderedMenu;
    Driver driver;
    Promo promo;
    HashMap<String, Promo> promoHashMap;
    HashMap<String, Driver> vacantDrivers;

    @BeforeEach
    public void setUp() {
        lineId = "id1";
        orderedMenu = new Menu("menu1");
        driver = new Driver("Test Driver", "Test");
        promo = new Promo("Test Promo", "Test", 2, 1000, 1);

        promoHashMap = new HashMap<>();
        promoHashMap.put(promo.getName(), promo);
        vacantDrivers = new HashMap<>();
        vacantDrivers.put("DV1", driver);
    }

    @Test
    public void testGetUserStateByLineId() {
        given(usersState.get(lineId)).willReturn(null)
                .willReturn(idleOrderState)
                .willReturn(processOrderState);

        OrderState userState = orderStateHelper.getUserStateByLineId(lineId);
        OrderState userState2 = orderStateHelper.getUserStateByLineId(lineId);

        then(usersState).should(times(4)).get(usersStateArgumentCaptor.capture());
        assertTrue(userState instanceof IdleOrderState);
        assertTrue(userState2 instanceof ProcessOrderState);

    }

    @Test
    public void testSetUserStateByLineId() {
        orderStateHelper.setUserStateByLineId(lineId, processOrderState);

        then(usersState).should().put(lineId, processOrderState);
    }

    @Test
    public void testGetUserMenuByLineId() {
        orderStateHelper.setUserMenuByLineId(lineId, orderedMenu);
        Menu userMenu = orderStateHelper.getUserMenuByLineId(lineId);

        then(usersOrderedMenu).should().get(lineId);
        assertNotNull(userMenu);
    }

    @Test
    public void testSetUserMenuByLineId() {
        orderedMenu.add(orderedMenu);
        orderStateHelper.setUserMenuByLineId(lineId, orderedMenu);
        Menu userMenu = orderStateHelper.getUserMenuByLineId(lineId);

        then(usersOrderedMenu).should().put(lineId, orderedMenu);
        assertNotNull(userMenu);
    }

    @Test
    public void testGetStateByStateName() {
        OrderState currentState = orderStateHelper.getStateByStateName(IdleOrderState.STATE_NAME);
        assertNotNull(currentState);
        assertTrue(currentState instanceof IdleOrderState);

        currentState = orderStateHelper.getStateByStateName(ProcessOrderState.STATE_NAME);
        assertNotNull(currentState);
        assertTrue(currentState instanceof ProcessOrderState);

        currentState = orderStateHelper.getStateByStateName(MakeOrderState.STATE_NAME);
        assertNotNull(currentState);
        assertTrue(currentState instanceof MakeOrderState);

        currentState = orderStateHelper.getStateByStateName(UsePromoState.STATE_NAME);
        assertNotNull(currentState);
        assertTrue(currentState instanceof UsePromoState);

        currentState = orderStateHelper.getStateByStateName("STATE");
        assertNull(currentState);
    }

    @Test
    public void getUserDriverByLineIdTest() {
        orderStateHelper.setUserDriverByLineId(lineId, driver);
        Driver userDriver = orderStateHelper.getUserDriverByLineId(lineId);

        then(usersDriver).should().get(lineId);
        assertNotNull(userDriver);
    }

    @Test
    public void setUserDriverByLineIdTest() {
        orderStateHelper.setUserDriverByLineId(lineId, driver);
        Driver userDriver = orderStateHelper.getUserDriverByLineId(lineId);

        then(usersDriver).should().put(lineId, driver);
        assertNotNull(userDriver);
    }

    @Test
    public void removeUserDriverByLineIdTest() {
        orderStateHelper.removeUserDriverByLineId(lineId);

        then(usersDriver).should().remove(lineId);
    }

    @Test
    public void getUserPromosTest() {
        given(usersPromo.get(lineId)).willReturn(promoHashMap);
        ArrayList<Promo> getPromo = orderStateHelper.getUserPromos(lineId);

        then(usersPromo).should().get(lineId);
    }

    @Test
    public void getPromoFromUserPromoTest() {
        given(usersPromo.get(lineId)).willReturn(promoHashMap);
        Promo getPromo = orderStateHelper.getPromoFromUserPromo(lineId, promo.getName());

        then(usersPromo).should().get(lineId);
        assertEquals(getPromo.getName(), promo.getName());
    }

    @Test
    public void getPromosByLineIdTest() {
        given(usersPromo.get(lineId)).willReturn(promoHashMap);
        ArrayList<Promo> getPromos = orderStateHelper.getPromosByLineId(lineId);

        then(usersPromo).should().get(lineId);

        assertNotNull(getPromos);
    }

    @Test
    public void setPromosByLineIdTest() {
        orderStateHelper.setPromosByLineId(lineId, promoHashMap);
        
        then(usersPromo).should().put(lineId, promoHashMap);

    }

    @Test
    public void useUserPromoTest() {
        given(usersPromo.get(lineId)).willReturn(promoHashMap);

        orderStateHelper.useUserPromo(lineId, promo.getName());

        promo.setCount(1);
        orderStateHelper.useUserPromo(lineId, promo.getName());

        then(usersPromo).should(times(3)).get(lineId);

    }

    @Test
    public void getUsedPromoTest() {
        given(usersUsedPromo.get(lineId)).willReturn(promo);

        Promo getPromo = orderStateHelper.getUsedPromo(lineId);
        assertEquals(getPromo.getName(), promo.getName());
    }

    @Test
    public void usedPromoTest() {
        given(usersPromo.get(lineId)).willReturn(promoHashMap);
        Promo getPromo = orderStateHelper.usedPromo(lineId, promo.getName());
        then(usersUsedPromo).should().put(lineId, promo);
        assertEquals(getPromo.getName(), promo.getName());
    }

    @Test
    public void cancelPromoTest() {
        orderStateHelper.cancelPromo(lineId);
        then(usersUsedPromo).should().remove(lineId);
    }

    @Test
    public void setInitialPromosByLineIdTest() {
        given(promoService.getPromosMap()).willReturn(promoHashMap);
        given(usersPromo.get(lineId)).willReturn(null).willReturn(promoHashMap);

        orderStateHelper.setInitialPromosByLineId(lineId);
        then(promoService).should().getPromosMap();
        then(usersPromo).should()
                .put(stringArgumentCaptor.capture(), mapStringPromoCaptor.capture());
        orderStateHelper.setInitialPromosByLineId(lineId);
        then(usersPromo).should(times(2)).get(lineId);
    }

    @Test
    public void getUserDriverTest() {
        given(driverService.getRandomVacantDriver()).willReturn(driver);

        Driver driverTest = orderStateHelper.getUserDriver();

        assertNotNull(driverTest);
    }

    @Test
    public void driverVacancyTest() {
        given(driverService.getVacantDriversList())
                .willReturn(new ArrayList<>())
                .willReturn(new ArrayList<>(vacantDrivers.values()));

        boolean isDriverAvailable = orderStateHelper.driverVacancy();
        assertFalse(isDriverAvailable);
        isDriverAvailable = orderStateHelper.driverVacancy();
        assertTrue(isDriverAvailable);
    }

    @Test
    public void vacantUserDriverTest() {
        orderStateHelper.vacantUserDriver(driver);
        then(driverService).should().vacantDriver(driver);
    }

    @Test
    public void getStringAllPromoTest() {
        given(promoService.getPromosString()).willReturn("All Promo");

        String promoString = orderStateHelper.getStringAllPromo();
        assertNotNull(promoString);
    }

}
