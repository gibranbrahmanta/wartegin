package com.herokuapp.wartegin.state;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import com.herokuapp.wartegin.composite.Menu;
import com.herokuapp.wartegin.composite.MenuItem;
import com.herokuapp.wartegin.messages.state.MakeOrderStateMessages;
import com.herokuapp.wartegin.model.Promo;
import com.herokuapp.wartegin.service.MenuService;
import com.herokuapp.wartegin.state.helper.OrderStateHelper;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;




@ExtendWith(MockitoExtension.class)
public class MakeOrderStateTest {

    @Mock
    IdleOrderState idleOrderState;

    @Mock
    ProcessOrderState processOrderState;

    @Mock
    UsePromoState usePromoState;

    @Mock
    MenuService menuService;

    @Mock
    OrderStateHelper orderStateHelper;

    @Spy
    ArrayList<Message> responses;

    @Mock
    MakeOrderStateMessages makeOrderStateMessages;

    @InjectMocks
    MakeOrderState makeOrderState;

    @Captor
    ArgumentCaptor<Message> messageArgumentCaptor;

    Menu orderedMenu;
    MenuItem menuItem;
    String lineId;
    Promo promo;


    @BeforeEach
    public void setUp() {
        orderedMenu = new Menu("menuList1");
        menuItem = new MenuItem("menuItem1", 1);
        lineId = "id1";

        promo = new Promo("promo", "test", 1, 1000, 1);
    }

    @Test
    public void testAddItem() {
        given(orderStateHelper.getUserMenuByLineId(lineId)).willReturn(orderedMenu);

        List<Message> addItemMessages = makeOrderState.addItem(menuItem, lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, addItemMessages.size());

    }

    @Test
    public void testDeleteItem() {
        given(orderStateHelper.getUserMenuByLineId(lineId)).willReturn(orderedMenu);
        given(makeOrderStateMessages.getDeleteItemMessage()).willReturn(
                new TextMessage("Your item has been successfully deleted from the order List"));
        orderedMenu.add(menuItem);

        List<Message> deleteItemMessage = makeOrderState.deleteItem(menuItem.getName(), lineId);

        then(responses).should(times(2)).add(messageArgumentCaptor.capture());
        assertEquals(2, deleteItemMessage.size());
    }

    @Test
    public void testMakeOrder() {
        given(makeOrderStateMessages.getMakeOrderMessage()).willReturn(new TextMessage(
                "You are currently making your Order, please add items to your Order List"));

        List<Message> makeOrderMessages = makeOrderState.makeOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, makeOrderMessages.size());

    }

    @Test
    public void testConfirmOrder() {
        ArrayList<Promo> promos = new ArrayList<>();
        promos.add(new Promo("promo 1", "ini promo 1", 1, 1, 1));
        promos.add(new Promo("promo 2", "ini promo 2", 2, 2, 2));

        given(orderStateHelper.getStateByStateName(UsePromoState.STATE_NAME))
                .willReturn(usePromoState);
        given(makeOrderStateMessages.getUsePromoMessage())
                .willReturn(
                        new TextMessage("You must 'Confirm Order' to use a promo for this Order!"));
        given(orderStateHelper.getPromosByLineId(lineId)).willReturn(promos);

        List<Message> confirmOrderMessages = makeOrderState.confirmOrder(lineId);

        then(responses).should(times(3)).add(messageArgumentCaptor.capture());
        assertEquals(3, confirmOrderMessages.size());
    }

    @Test
    public void testCancelOrder() {
        given(orderStateHelper.getStateByStateName(IdleOrderState.STATE_NAME))
                .willReturn(idleOrderState);
        given(makeOrderStateMessages.getCancelOrderMessage())
                .willReturn(new TextMessage("Your Order has been successfully cancelled"));

        List<Message> confirmOrderMessages = makeOrderState.cancelOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, confirmOrderMessages.size());
    }

    @Test
    public void testFinishOrder() {
        given(makeOrderStateMessages.getFinishOrderMessage()).willReturn(new TextMessage(
                "You must 'Confirm Order' then wait for your Order to be processed by system"));

        List<Message> finishOrderMessages = makeOrderState.finishOrder(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, finishOrderMessages.size());
    }

    @Test
    public void testShowOrderList() {
        given(orderStateHelper.getUserMenuByLineId(lineId)).willReturn(orderedMenu);
        orderedMenu.add(menuItem);
        orderedMenu.add(menuItem);

        List<Message> showOrderListMessages = makeOrderState.showOrderList(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(1, showOrderListMessages.size());
    }

    @Test
    public void testUsedPromo() {
        given(makeOrderStateMessages.getUsedPromoMessage())
                .willReturn(new TextMessage(
                        "You must 'Confirm Order' to use a promo for this Order!"));
        List<Message> usedPromoMessage = makeOrderState.usedPromo(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(makeOrderStateMessages.getUsedPromoMessage(),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) usedPromoMessage.get(0);
        assertEquals(makeOrderStateMessages.getUsedPromoMessage().getText(), message.getText());

        assertEquals(1, usedPromoMessage.size());
    }

    @Test
    public void testUsePromo() {
        given(makeOrderStateMessages.getUsePromoMessage())
                .willReturn(new TextMessage(
                        "You must 'Confirm Order' to use a promo for this Order!"));
        List<Message> usePromoMessage = makeOrderState.usePromo(lineId, promo.getName());

        then(responses).should().add(messageArgumentCaptor.capture());
        assertEquals(makeOrderStateMessages.getUsePromoMessage(),
                messageArgumentCaptor.getValue());

        TextMessage message = (TextMessage) usePromoMessage.get(0);
        assertEquals(makeOrderStateMessages.getUsePromoMessage().getText(), message.getText());

        assertEquals(1, usePromoMessage.size());
    }

    @Test
    public void testCancelPromo() {
        given(makeOrderStateMessages.getCancelPromoMessage())
                .willReturn(new TextMessage(
                        "You can't use promo. You have already checked out your Order"));
        List<Message> cancelPromoMessages = makeOrderState.cancelPromo(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertTrue(messageArgumentCaptor.getValue() instanceof TextMessage);
        assertEquals(1, cancelPromoMessages.size());
    }

    @Test
    public void testSeePromo() {
        ArrayList<Promo> listOfPromos = new ArrayList<>();
        listOfPromos.add(promo);
        listOfPromos.add(promo);
        given(orderStateHelper.getUserPromos(lineId)).willReturn(listOfPromos);

        List<Message> seePromoMessages = makeOrderState.seePromo(lineId);

        then(responses).should().add(messageArgumentCaptor.capture());
        assertNotNull(messageArgumentCaptor.getValue());
        assertEquals(seePromoMessages.size(), 1);
    }
}
