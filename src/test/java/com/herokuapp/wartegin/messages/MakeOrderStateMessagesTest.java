package com.herokuapp.wartegin.messages;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.herokuapp.wartegin.messages.state.MakeOrderStateMessages;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MakeOrderStateMessagesTest {

    @Spy
    MakeOrderStateMessages makeOrderStateMessages;

    @Test
    public void testGetAddItemMessage() {
        TextMessage addItemMessage = makeOrderStateMessages.getAddItemMessage();
        assertNull(addItemMessage);
    }

    @Test
    public void testGetDeleteItemMessage() {
        TextMessage deleteItemMessage = makeOrderStateMessages.getDeleteItemMessage();
        assertNotNull(deleteItemMessage);
    }

    @Test
    public void testGetFinishOrderMessage() {
        TextMessage finishOrderMessage = makeOrderStateMessages.getFinishOrderMessage();
        assertNotNull(finishOrderMessage);
    }

    @Test
    public void testGetConfirmOrderMessage() {
        TextMessage confirmOrderMessage = makeOrderStateMessages.getConfirmOrderMessage();
        assertNotNull(confirmOrderMessage);
    }

    @Test
    public void testGetCancelOrderMessage() {
        TextMessage cancelOrderMessage = makeOrderStateMessages.getCancelOrderMessage();
        assertNotNull(cancelOrderMessage);
    }

    @Test
    public void testGetMakeOrderMessage() {
        TextMessage makeOrderMessage = makeOrderStateMessages.getMakeOrderMessage();
        assertNotNull(makeOrderMessage);
    }

    @Test
    public void testGetShowOrderMessage() {
        TextMessage showOrderMessage = makeOrderStateMessages.getShowOrderMessage();
        assertNull(showOrderMessage);
    }

    @Test
    public void getTryAgainLaterMessage() {
        TextMessage tryAgainLaterMessage = makeOrderStateMessages.getTryAgainLaterMessage();
        assertNotNull(tryAgainLaterMessage);
    }

    @Test
    public void getUsePromoMessage() {
        TextMessage usePromoMessage = makeOrderStateMessages.getUsePromoMessage();
        assertNotNull(usePromoMessage);
    }

    @Test
    public void getUsedPromoMessage() {
        TextMessage usedPromoMessage = makeOrderStateMessages.getUsedPromoMessage();
        assertNotNull(usedPromoMessage);
    }

    @Test
    public void getCancelPromoMessage() {
        TextMessage cancelPromoMessage = makeOrderStateMessages.getCancelPromoMessage();
        assertNotNull(cancelPromoMessage);
    }
}
