package com.herokuapp.wartegin.messages;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.herokuapp.wartegin.messages.state.PromoOrderStateMessages;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PromoOrderStateMessagesTest {

    @Spy
    PromoOrderStateMessages promosOrderStateMessages;


    @Test
    public void testGetAddItemMessage() {
        TextMessage addItemMessage = promosOrderStateMessages.getAddItemMessage();
        assertNotNull(addItemMessage);
    }

    @Test
    public void testGetDeleteItemMessage() {
        TextMessage deleteItemMessage = promosOrderStateMessages.getDeleteItemMessage();
        assertNotNull(deleteItemMessage);
    }

    @Test
    public void testGetFinishOrderMessage() {
        TextMessage finishOrderMessage = promosOrderStateMessages.getFinishOrderMessage();
        assertNotNull(finishOrderMessage);
    }

    @Test
    public void testGetConfirmOrderMessage() {
        TextMessage confirmOrderMessage = promosOrderStateMessages.getConfirmOrderMessage();
        assertNotNull(confirmOrderMessage);
    }

    @Test
    public void testGetCancelOrderMessage() {
        TextMessage cancelOrderMessage = promosOrderStateMessages.getCancelOrderMessage();
        assertNotNull(cancelOrderMessage);
    }

    @Test
    public void testGetMakeOrderMessage() {
        TextMessage makeOrderMessage = promosOrderStateMessages.getMakeOrderMessage();
        assertNotNull(makeOrderMessage);
    }

    @Test
    public void testGetShowOrderMessage() {
        TextMessage showOrderMessage = promosOrderStateMessages.getShowOrderMessage();
        assertNull(showOrderMessage);
    }

    @Test
    public void getTryAgainLaterMessage() {
        TextMessage tryAgainLaterMessage = promosOrderStateMessages.getTryAgainLaterMessage();
        assertNotNull(tryAgainLaterMessage);
    }

    @Test
    public void getUsePromoMessage() {
        TextMessage usePromoMessage = promosOrderStateMessages.getUsePromoMessage();
        assertNull(usePromoMessage);
    }

    @Test
    public void getUsedPromoMessage() {
        TextMessage usedPromoMessage = promosOrderStateMessages.getUsedPromoMessage();
        assertNotNull(usedPromoMessage);
    }

    @Test
    public void getCancelPromoMessage() {
        TextMessage cancelPromoMessage = promosOrderStateMessages.getCancelPromoMessage();
        assertNotNull(cancelPromoMessage);
    }
}
