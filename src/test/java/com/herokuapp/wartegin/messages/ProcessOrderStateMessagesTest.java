package com.herokuapp.wartegin.messages;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.herokuapp.wartegin.messages.state.ProcessOrderStateMessages;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProcessOrderStateMessagesTest {

    @Spy
    ProcessOrderStateMessages processOrderStateMessages;

    @Test
    public void testGetAddItemMessage() {
        TextMessage addItemMessage = processOrderStateMessages.getAddItemMessage();
        assertNotNull(addItemMessage);
    }

    @Test
    public void testGetDeleteItemMessage() {
        TextMessage deleteItemMessage = processOrderStateMessages.getDeleteItemMessage();
        assertNotNull(deleteItemMessage);
    }

    @Test
    public void testGetFinishOrderMessage() {
        TextMessage finishOrderMessage = processOrderStateMessages.getFinishOrderMessage();
        assertNotNull(finishOrderMessage);
    }

    @Test
    public void testGetConfirmOrderMessage() {
        TextMessage confirmOrderMessage = processOrderStateMessages.getConfirmOrderMessage();
        assertNotNull(confirmOrderMessage);
    }

    @Test
    public void testGetCancelOrderMessage() {
        TextMessage cancelOrderMessage = processOrderStateMessages.getCancelOrderMessage();
        assertNotNull(cancelOrderMessage);
    }

    @Test
    public void testGetMakeOrderMessage() {
        TextMessage makeOrderMessage = processOrderStateMessages.getMakeOrderMessage();
        assertNotNull(makeOrderMessage);
    }

    @Test
    public void testGetShowOrderMessage() {
        TextMessage showOrderMessage = processOrderStateMessages.getShowOrderMessage();
        assertNull(showOrderMessage);
    }

    @Test
    public void getTryAgainLaterMessage() {
        TextMessage tryAgainLaterMessage = processOrderStateMessages.getTryAgainLaterMessage();
        assertNotNull(tryAgainLaterMessage);
    }

    @Test
    public void getUsePromoMessage() {
        TextMessage usePromoMessage = processOrderStateMessages.getUsePromoMessage();
        assertNotNull(usePromoMessage);
    }

    @Test
    public void getUsedPromoMessage() {
        TextMessage usedPromoMessage = processOrderStateMessages.getUsedPromoMessage();
        assertNotNull(usedPromoMessage);
    }

    @Test
    public void getCancelPromoMessage() {
        TextMessage cancelPromoMessage = processOrderStateMessages.getCancelPromoMessage();
        assertNotNull(cancelPromoMessage);
    }
}
