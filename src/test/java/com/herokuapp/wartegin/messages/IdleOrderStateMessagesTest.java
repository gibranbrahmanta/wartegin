package com.herokuapp.wartegin.messages;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.herokuapp.wartegin.messages.state.IdleOrderStateMessages;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class IdleOrderStateMessagesTest {

    @Spy
    IdleOrderStateMessages idleOrderStateMessages;

    @Test
    public void testGetAddItemMessage() {
        TextMessage addItemMessage = idleOrderStateMessages.getAddItemMessage();
        assertNotNull(addItemMessage);
    }

    @Test
    public void testGetDeleteItemMessage() {
        TextMessage deleteItemMessage = idleOrderStateMessages.getDeleteItemMessage();
        assertNotNull(deleteItemMessage);
    }

    @Test
    public void testGetFinishOrderMessage() {
        TextMessage finishOrderMessage = idleOrderStateMessages.getFinishOrderMessage();
        assertNotNull(finishOrderMessage);
    }

    @Test
    public void testGetConfirmOrderMessage() {
        TextMessage confirmOrderMessage = idleOrderStateMessages.getConfirmOrderMessage();
        assertNotNull(confirmOrderMessage);
    }

    @Test
    public void testGetCancelOrderMessage() {
        TextMessage cancelOrderMessage = idleOrderStateMessages.getCancelOrderMessage();
        assertNotNull(cancelOrderMessage);
    }

    @Test
    public void testGetMakeOrderMessage() {
        TextMessage makeOrderMessage = idleOrderStateMessages.getMakeOrderMessage();
        assertNotNull(makeOrderMessage);
    }

    @Test
    public void testGetShowOrderMessage() {
        TextMessage showOrderMessage = idleOrderStateMessages.getShowOrderMessage();
        assertNotNull(showOrderMessage);
    }

    @Test
    public void getTryAgainLaterMessage() {
        TextMessage tryAgainLaterMessage = idleOrderStateMessages.getTryAgainLaterMessage();
        assertNotNull(tryAgainLaterMessage);
    }

    @Test
    public void getUsePromoMessage() {
        TextMessage usePromoMessage = idleOrderStateMessages.getUsePromoMessage();
        assertNotNull(usePromoMessage);
    }

    @Test
    public void getUsedPromoMessage() {
        TextMessage usedPromoMessage = idleOrderStateMessages.getUsedPromoMessage();
        assertNotNull(usedPromoMessage);
    }

    @Test
    public void getCancelPromoMessage() {
        TextMessage cancelPromoMessage = idleOrderStateMessages.getCancelPromoMessage();
        assertNotNull(cancelPromoMessage);
    }


}
