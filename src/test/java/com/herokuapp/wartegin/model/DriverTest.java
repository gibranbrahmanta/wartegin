package com.herokuapp.wartegin.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class DriverTest {

    private Driver driver;

    @BeforeEach
    public void setUp() {
        String name = "Test";
        String plateNumber = "Test";
        driver = new Driver(name, plateNumber);
    }

    @Test
    void getName() {
        assertEquals("Test", driver.getName());
    }


    @Test
    void getPlateNumber() {
        assertEquals("Test", driver.getPlateNumber());
    }


}
