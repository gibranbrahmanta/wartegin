package com.herokuapp.wartegin.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MenuTest {

    private Menu menu;

    @BeforeEach
    public void setUp() {
        menu = new Menu();
        menu.setName("Test");
        menu.setId(1);
        menu.setPrice(1);
        menu.setType(1);
    }

    @Test
    void getName() {
        assertEquals("Test", menu.getName());
    }

    @Test
    void setName() {
        menu.setName("Test Test");
        assertEquals("Test Test", menu.getName());
    }

    @Test
    void getId() {
        assertEquals(1, menu.getId());
    }

    @Test
    void setId() {
        menu.setId(2);
        assertEquals(2, menu.getId());
    }

    @Test
    void getPrice() {
        assertEquals(1, menu.getPrice());
    }

    @Test
    void setPrice() {
        menu.setPrice(2);
        assertEquals(2, menu.getPrice());
    }

    @Test
    void getType() {
        assertEquals(1, menu.getType());
    }

    @Test
    void setType() {
        menu.setType(2);
        assertEquals(2, menu.getType());
    }

}
