package com.herokuapp.wartegin.model;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class PromoTest {

    private Promo promo;

    @BeforeEach
    public void setUp() {
        String name = "Test";
        String description = "Test";
        long count = 1;
        long amount = 1000;
        double percent = 1;
        promo = new Promo(name, description, count, amount, percent);
    }

    @Test
    void getName() {
        assertEquals("Test", promo.getName());
    }


    @Test
    void setId() {
        promo.setId("Test Test");
        assertEquals("Test Test", promo.getName());
    }

    @Test
    void getDescription() {
        assertEquals("Test", promo.getDescription());
    }


    @Test
    void setDescription() {
        promo.setDescription("Test Test");
        assertEquals("Test Test", promo.getDescription());
    }

    @Test
    void getCount() {
        assertEquals((long) 1, promo.getCount());
    }


    @Test
    void setCount() {
        promo.setCount((long) 2);
        assertEquals((long) 2, promo.getCount());
    }

    @Test
    void addCount() {
        promo.setCount((long) 1);
        promo.addCount((long) 2);
        assertEquals((long) 3, promo.getCount());
    }

    @Test
    void getAmount() {
        assertEquals((long) 1000, promo.getAmount());
    }


    @Test
    void setAmount() {
        promo.setAmount((long) 2000);
        assertEquals((long) 2000, promo.getAmount());
    }

    @Test
    void getPercent() {
        assertEquals((double) 1, promo.getPercent());
    }


    @Test
    void setPercent() {
        promo.setPercent((double) 0.5);
        assertEquals((double) 0.5, promo.getPercent());
    }


}
