package com.herokuapp.wartegin.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class LineUserTest {

    private LineUser user;

    @BeforeEach
    public void setUp() {
        user = new LineUser();
        user.setName("Test");
        user.setLineId("1");
    }

    @Test
    void getName() {
        assertEquals("Test", user.getName());
    }

    @Test
    void setName() {
        user.setName("Test Test");
        assertEquals("Test Test", user.getName());
    }

    @Test
    void getId() {
        assertEquals("1", user.getLineId());
    }

    @Test
    void setId() {
        user.setLineId("2");
        assertEquals("2", user.getLineId());
    }

}
