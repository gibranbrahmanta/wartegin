package com.herokuapp.wartegin.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import com.herokuapp.wartegin.model.LineUser;
import com.herokuapp.wartegin.repository.LineUserRepository;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class LineUserServiceTest {

    @Mock
    private LineUserRepository lineUserRepository;

    private LineUser user;

    @InjectMocks
    private LineUserServiceImplementation lineUserService;

    @BeforeEach
    public void setUp() {
        user = new LineUser();
        user.setName("Test");
        user.setLineId("1");
    }

    @Test
    public void testFindAll() {
        List<LineUser> userList = lineUserService.findAll();
        lenient().when(lineUserService.findAll()).thenReturn(userList);
    }

    @Test
    public void testRegisterUser() {
        lineUserService.register(user);
        lenient().when(lineUserService.register(user)).thenReturn(user);
    }

    @Test
    public void testFindUser() {
        lineUserService.register(user);
        Optional<LineUser> optionalUserExist = lineUserService.findUserById(user.getLineId());
        Optional<LineUser> optionalUserDoesntExist = lineUserService.findUserById("2");
        lenient().when(lineUserService.findUserById(user.getLineId()))
                .thenReturn(Optional.of(user));
    }

    @Test
    public void testEraseUser() {
        lineUserService.register(user);
        lineUserService.erase(user.getLineId());
        lenient().when(lineUserService.findUserById(user.getLineId())).thenReturn(Optional.empty());
    }

    @Test
    public void testRewriteUser() {
        when(lineUserRepository.save(user)).thenReturn(user);
        lineUserService.register(user);
        assertEquals(user, lineUserService.rewrite(user));
    }

    @Test
    public void testIsValid() {
        boolean isValid = lineUserService.isValid(user);
        assertTrue(isValid);
    }

    @Test
    public void testIsValid2() {
        boolean isValid = lineUserService.isValid(null);
        assertFalse(isValid);
    }
}

