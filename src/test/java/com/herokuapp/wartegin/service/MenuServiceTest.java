package com.herokuapp.wartegin.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;

import com.herokuapp.wartegin.model.Menu;
import com.herokuapp.wartegin.repository.MenuRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MenuServiceTest {

    @Mock
    private MenuRepository menuRepository;

    private Menu menu;
    private List<Menu> menuList;

    @InjectMocks
    private MenuServiceImplementation menuService;

    @BeforeEach
    public void setUp() {
        menu = new Menu();
        menu.setName("Test");
        menu.setId(1);
        menu.setPrice(1);
        menu.setType(1);
        menuList = new ArrayList<>();
    }

    @Test
    public void testFindAll() {
        menuList.add(menu);
        menuList.add(menu);
        when(menuRepository.findAll()).thenReturn(menuList);

        List<Menu> allMenu = menuService.findAll();

        assertEquals(2, allMenu.size());
    }

    @Test
    public void testRegisterMenu() {
        when(menuRepository.save(menu)).thenReturn(menu);
        Menu savedMenu = menuService.register(menu);
        assertNotNull(savedMenu);
    }

    @Test
    public void testFindMenu() {
        when(menuRepository.findById(menu.getId())).thenReturn(Optional.of(menu));

        Optional<Menu> optionalMenu = menuService.findMenu(menu.getId());
        assertNotNull(optionalMenu);
    }

    @Test
    public void testEraseMenu() {

        menuService.erase(menu.getId());
        then(menuRepository).should().deleteById(menu.getId());

    }

    @Test
    public void testRewriteMenu() {
        when(menuRepository.save(menu)).thenReturn(menu);
        Menu rewriteMenu = menuService.rewrite(menu);

        assertNotNull(rewriteMenu);
        then(menuRepository).should().save(menu);
    }

    @Test
    public void testFindAllFood() {
        menuList.add(menu);
        menuList.add(menu);

        given(menuRepository.findAllFood()).willReturn(menuList);

        List<Menu> foodList = menuService.findAllFood();

        then(menuRepository).should().findAllFood();
        assertEquals(2, foodList.size());
    }

    @Test
    public void testFindAllDrink() {
        menuList.add(menu);
        menuList.add(menu);

        given(menuRepository.findAllDrink()).willReturn(menuList);

        List<Menu> drinkList = menuService.findAllDrink();

        then(menuRepository).should().findAllDrink();
        assertEquals(2, drinkList.size());
    }

    @Test
    public void testIsValid() {
        boolean isValid = menuService.isValid(menu);
        assertTrue(isValid);

        menu = new Menu("Test",1,2);
        isValid = menuService.isValid(menu);
        assertTrue(isValid);

        menu = new Menu("",-1,0);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);

        menu = new Menu("",-1,1);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);
        menu = new Menu("",-1,2);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);

        menu = new Menu("",-1,0);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);

        menu = new Menu("",2,1);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);
        menu = new Menu("",2,2);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);

        menu = new Menu("A",-1,1);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);
        menu = new Menu("A",-1,2);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);

        menu = new Menu("A",1,0);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);

        menu = new Menu("A",-1,0);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);

        menu = new Menu("",1,0);
        isValid = menuService.isValid(menu);
        assertFalse(isValid);

        isValid = menuService.isValid(null);
        assertFalse(isValid);
    }
}

