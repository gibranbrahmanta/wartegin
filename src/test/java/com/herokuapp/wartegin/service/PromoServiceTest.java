package com.herokuapp.wartegin.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import com.herokuapp.wartegin.model.Promo;
import com.herokuapp.wartegin.repository.PromoRepository;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PromoServiceTest {

    @Spy
    private PromoRepository repository;

    @InjectMocks
    private PromoServiceImplementation promoService;

    @Test
    public void testGetPromosList() {
        assertEquals(3,promoService.getPromosList().size());
    }

    @Test
    public void testGetPromosMap() {
        assertEquals(3,promoService.getPromosMap().size());
    }

    @Test
    public void testGetPromosString() {
        assertNotEquals("",promoService.getPromosString());
    }

    @Test
    public void testAddPromoToRepository() {
        Promo promo = new Promo("Ramadhan 50%", "Get 50% discount for all items!", 3, 0, 0.5);
        repository.addPromo(promo);
        repository.addPromo(promo);
    }

}

