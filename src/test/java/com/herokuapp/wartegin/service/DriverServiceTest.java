package com.herokuapp.wartegin.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import com.herokuapp.wartegin.model.Driver;
import com.herokuapp.wartegin.repository.DriverRepository;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DriverServiceTest {

    @Spy
    private DriverRepository repository;

    private Driver driver = new Driver("Gibran","B6441VKE");

    @InjectMocks
    private DriverServiceImplementation driverService;

    @Test
    public void testGetDrivers() {
        assertEquals(10,driverService.getDrivers().size());
    }

    @Test
    public void testGetRandomVacantDriver() {
        assertNotNull(driverService.getRandomVacantDriver());
    }

    @Test
    public void testVacantDriver() {
        Driver vacant = driverService.vacantDriver(driver);
        assertEquals("Gibran",vacant.getName());

        vacant = driverService.vacantDriver(driver);
        assertNotNull(vacant);

        then(repository).should(times(2)).addVacantDriver(driver);
    }

    @Test
    public void testOccupyDriver() {
        Driver occupy = driverService.occupyDriver("Gibran");
        assertNull(occupy);
    }

    @Test
    public void testGetVacantDriversList() {
        ArrayList<Driver> driverArrayList = repository.getVacantDriversList();
        assertNotNull(driverArrayList);
    }
}

